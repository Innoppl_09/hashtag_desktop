<!DOCTYPE html>
<html>
  <head>

    <meta charset="utf-8">
    <title>twitter - Embedded tweets</title>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://npmcdn.com/masonry-layout@4.0/dist/masonry.pkgd.js"></script>
<style>
.grid {
  width:100%;
  max-width: 1050px;
  margin: 50px auto;
}
.grid-load-more {
      min-width: 180px;
    margin: 0 auto;
    display: block;
    max-width: 120px;
  
}
.loadmore{
    padding: 20px 50px;
    background: #1da1f2;
    color: white;
    font-weight: 600;
    font-size: 16px;
    border: none;
}
.grid-item {
  width:250px;
  float:left;
  
}
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid blue;
  border-right: 16px solid green;
  border-bottom: 16px solid red;
  border-left: 16px solid pink;
  width: 20px;
  height: 20px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
  </head>

  <body>
    <div class="grid">


      

    </div>
 
 <div class="grid-load-more">
   <div class="loader"></div>
<button class="loadmore" onclick="loadmore_twitter();">Click me</button>

</div>

    <!-- Peace be upon you / ziegfiroyt -->

  </body>

<script>
// global variable 
var tweetdata = ["966718775499227136","966714542024671232","966712152647061505","966710861338361856","966707577718386688",
"966706135276244993","966701131501010944","966697655689621504","966694933498281986","966692431222755330","965576464732110848",
"965575228964323328","965574650250932229","965573649926316034","965572608602206208","965572294276915200","965570390801051648",
"965569045687881728","965567247526739968","965566886682308611","964182523814215680","964177904455188481","964177247409074177",
"964176261265342464","964174511049392128","964171662613667840","964170235837267969","964169531429040128","964169498231160833",
"964169467088453632","964121920420462594","964121697472339968","964119903954337792","964118768262639616","964117744743473152",
"964116227634356229","964116116472659969","964115985199493120","964113450178789377","964112583711059968","964105940860510208",
"964105415708499968","964104716190810112","964102423391346690","964101854895407104","964098312461737984","964097686667317248",
"964096954111213569","964095696461705216","964093270639853568","964056070044844032","964055463548596224","964055267611684865",
"964054778790727680","964054346273927173","964054014651375616","964048671095566336","964048134362943488","964043056507846656",
"964040183287988224","963821963977351169","963821666458517504","963820044953182209","963809546840887296","963803996220620800",
"963802587106545664","963801828285534210","963798208479571969","963796081384464384","963793718695399424","963729405930479616",
"963726435687940096","963725443617587208","963723687730954240","963721441785954304","963717887616630784","963717214154063873","963717163755323392",
"963714176056156160","963713948146024448","961985869052747776","961984935018995712","961983796433203201","961982187712102400","961981656902111233",
"961981052402262018","961979832946118656","961979729497722881","961976510214123520","961976060207181828","961964176678699009","961963137560854528",
"961962964847771648","961962793502105600","961962431034441728","961961061632143360","961959963332509706","961959197410697216","961958544034549763",
"961953093658144774","961941776432095233","961941758404784128","961941602800291845","961941380737085440","961940997457330177",
"961940819593875456","961939778517196800","961938775961092096","961938319843053568","961938093640003585","961924884245880833","961922496130727936",
"961918621336272896","961914974237999106","961911899989336065","961910406787706881","961909504202874880","961909008301834241","961908937422286848",
"961908194674032640","961907509446324224","961905341674930176","961901873589399553","961892112634494976","961890986631843840","961890462201085953",
"961887890706976768","961887140455047168","961883082679726080","961879814654185472","961875085362528257","961873713061744640","961873404348395527",
"961873264174772224","961871199931310080","961869071879847936","961866259745751041","961864078661599233","961848463024521216","961845433776553989",
"961621795446099968","961621500515188737","961621042212098049","961620509543862279","961620460478844931","961616647902576640","961615292236378113",
"961610156059500545","961604318691393538","961603181993123841"];

var start=0;
var limit=15;

/* function for loading twitter content */

function pull_twitter_data() {
 Limits = start + limit;
 var items = tweetdata.slice(start, Limits);
 for(i = 0; i < items.length; i++) { 
  $(".grid").append('<div class="grid-item"><blockquote class="twitter-tweet"><a href="https://twitter.com/FoxBusiness/status/'+ items[i] +'"></a></blockquote></div>');  
 }
// increse the limit
 start = start + limit;
 if(Limits >= tweetdata.length){
    $(".loadmore").hide();
 }
}



/* load twitter script */

function load_twitter(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
 // if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);

  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };

  return t; 

}

/* load masonary script */

function load_masonary(twttr){
twttr.ready(function (twttr) {
  twttr.events.bind('loaded', function (event) {
    $('.grid').masonry({
      itemSelector : '.grid-item',
      columnWidth : 250,
      gutter: 15
    });
  });
});
}

/* reload masonary script */

function reload_masonary(twttr){


  twttr.events.bind('loaded', function (event) {
    $('.grid').masonry( 'reloadItems' );
    $('.grid').masonry( 'layout' );
  });

}


/* Pull twitter when document ready */
$( document ).ready(function() {

  pull_twitter_data();
  twttr=load_twitter(document, 'script', 'twitter-wjs');
  load_masonary(twttr);
  $('.loader').hide(2000);
   
  
});

/* Pull twitter when load more */

function loadmore_twitter(){
 $('.loader').show();
pull_twitter_data();
twttr=load_twitter(document, 'script', 'twitter-wjs');
reload_masonary(twttr);
 $('.loader').hide(2000);
}


</script>

</html>

