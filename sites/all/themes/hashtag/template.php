<?php
/**
 * @file
 * The primary PHP file for this theme.
 */

function hashtag_preprocess_page(&$vars, $hook) {
  if (isset($vars['node'])) {
    if (isset($vars['node']->type)) {
      $nodetype = $vars['node']->type;
      $vars['theme_hook_suggestions'][] = 'page__' . $nodetype;
      $vars['theme_hook_suggestions'][] = 'page__node__' . str_replace('_', '--', $vars['node']->type);
      $vars['theme_hook_suggestions'][] = 'page__node__' . $vars['node']->nid;
    }
  }
}
function hashtag_preprocess_html(&$vars, $hook) {
  global $base_url;
  if (drupal_is_front_page()) {
    $page_keywords = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => array(
        'name' => 'fo-verify',
        'content' => '70444ee0-e9a4-4438-8b4b-5dcd8fab93b1',
      )
    );
    drupal_add_html_head($page_keywords, 'page_keywords');
  }


 $node = menu_get_object();

  if ($node && isset($node->nid)) {
    $node = node_load($node->nid);
    $auhtor = $node->name;
    $datepublished = format_date($node->created);
    $node_path = drupal_get_path_alias(current_path());
    $node_title = $node->title;
    $logo_url = $base_url.'/sites/all/themes/hashtag/logo.png';
    /* twitter content for individual article page */
    if ($node->type == 'add_news_article') {
      $nodecategoryid = $node->field_news_category['und'][0]['tid'];
      if (isset($nodecategoryid) && !empty($nodecategoryid)) {
        $jssettings = array('nodecategoryid' => $nodecategoryid);
        drupal_add_js(array('hashtag' => $jssettings), 'setting');
      }
    }

    // watchdog("metatag", '<pre>' . print_r($node, true) . '</pre>'); 
    //     watchdog("metatag", '<pre>' . print_r($node_path, true) . '</pre>');  


$metatag_author = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => array(
        'itemprop' => 'author',
        'content' => $auhtor,
      )
    );
    drupal_add_html_head($metatag_author, 'author');

  $metatag_datepublished = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => array(
        'itemprop' => 'datePublished',
        'content' => $datepublished,
      )
    );
    drupal_add_html_head($metatag_datepublished, 'datePublished');

   
  $metatag_publisher = array(
      '#type' => 'markup',
      //'#tag' => '<meta>',
      // '#attributes' => array(
      //   'itemprop' => 'publisher',
      //   'itemtype' => 'https://schema.org/Organization',
       '#markup' => "<div itemprop='publisher' itemscope='' itemtype='https://schema.org/Organization'>
<div itemprop='logo' itemscope='' itemtype='https://schema.org/ImageObject' style='display:none;'>
<img src='$logo_url'/>
<meta content='$logo_url' itemprop='url'/>
<meta content='147' itemprop='width'/>
<meta content='88' itemprop='height'/>
</div>
<meta content='The Hashtag News' itemprop='name'/>
</div>",
        
        //) 

    );  
    drupal_add_html_head($metatag_publisher, 'publisher');

  }  
}


function hashtag_preprocess_user_login(&$vars) {
  $vars['destination'] = false;
  $vars['intro_text'] = t('This is my awesome login form');
}
function hashtag_preprocess_user_pass(&$vars) {
  $vars['destination'] = false;
  $vars['intro_text'] = t('This is my super awesome request new password form');
}
function hashtag_preprocess_user_register(&$vars) {
  $vars['destination'] = false;
  $vars['intro_text'] = t('This is my awesome Registration form');
}





function hashtag_theme() {
  return array(
    'twitter_pull_listing_2' => array(
      'arguments' => array('tweets' => NULL, 'twitkey' => NULL, 'title' => NULL),
      'template' => 'twitter-pull-listing-2'
    ),
    'user_login' => array(
      'template' => 'user-login',
      'render element' => 'form',
    'path' => drupal_get_path('theme', 'hashtag') . '/templates'
    ),
    'user_pass' => array(
      'template' => 'user-pass',
      'render element' => 'form',
    'path' => drupal_get_path('theme', 'hashtag') . '/templates'
    ),
    'user_register_form' => array(
      'template' => 'user-register',
      'render element' => 'form',
    'path' => drupal_get_path('theme', 'hashtag') . '/templates'
    ),
  );
}

function hashtag_preprocess_node(&$variables) {
  $node = $variables['node'];
  if($node->type == "add_news_article") {
    if (isset($node->field_display_type['und'][0]['value']) && !empty($node->field_display_type['und'][0]['value'])) {
      $variables['slide_flag'] = 1;
      if (isset($node->field_gallery_preview_image['und'][0]['uri']) && !empty($node->field_gallery_preview_image['und'][0]['uri'])) {
        $variables['gall_preview'] = 1;
        $variables['prev_created'] = format_date($node->created, 'custom', 'M j,Y');
        $gall_view = views_get_view('gallery_view_slider');
        $gall_view->set_display('gallery');
        $gall_view->set_arguments(array($node->nid));
        $gall_view->execute();
        $gall_cnt = count($gall_view->result);
        $variables['slider_count'] = $gall_cnt . ' Photos';
      }
      else {
        $variables['gall_preview'] = 0;
      }
    }
    else {
      $variables['slide_flag'] = 0;
    }
    if (isset($node->field_author['und'][0]['value']) && !empty($node->field_author['und'][0]['value'])) {
      $variables['node_author'] = "By: " . $node->field_author['und'][0]['value'];
    }
    $variables['node_created'] = format_date($node->created, $type = 'long');
  }
}








