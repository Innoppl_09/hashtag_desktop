<?php
/**
 * @file
 * Default view template to display content in a Masonry layout.
 */
?>

<?php if (isset($grouping) && $grouping): ?>
  <?php if (!empty($title)): ?>
    <h3 style="clear:both;"><?php print $title; ?></h3>
    <?php endif; ?>
  <?php print $prefix ?>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div class="masonry-item<?php if ($classes_array[$id]) print ' ' . $classes_array[$id]; ?>">
    <?php print $row; ?>
  </div>
  <?php if ($id+1 == 3): ?>
    <div class="masonry-item<?php if ($classes_array[$id]) print ' ' . $classes_array[$id]; ?> row-ad">
      <div class="views-field views-field-nothing"><div id='Incontent3' class="banner-ad"></div></div>
    </div>
  <?php elseif ($id+1 == 6): ?>
    <div class="masonry-item<?php if ($classes_array[$id]) print ' ' . $classes_array[$id]; ?> row-ad">
      <div class="views-field views-field-nothing"><div id='Incontent4' class="banner-ad"></div></div>
    </div>
  <?php elseif ($id+1 == 9): ?>
    <div class="masonry-item<?php if ($classes_array[$id]) print ' ' . $classes_array[$id]; ?> row-ad">
      <div class="views-field views-field-nothing"><div id='Incontent5' class="banner-ad"></div></div>
    </div>
  <?php elseif ($id+1 == 12): ?>
    <div class="masonry-item<?php if ($classes_array[$id]) print ' ' . $classes_array[$id]; ?> row-ad">
      <div class="views-field views-field-nothing"><div id='Incontent6' class="banner-ad"></div></div>
    </div>
  <?php endif; ?>
<?php endforeach; ?>
<?php if(isset($grouping) && $grouping): ?>
  <?php print $suffix ?>
<?php endif;?>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
<script async src="//static.addtoany.com/menu/page.js"></script>

