(function($) {
  if (screen.width < 768) {
    if(document.location.host == 'thehashtag.news' || document.location.host == 'www.thehashtag.news' ){
      document.location = "https://m.thehashtag.news" + document.location.pathname;
    }
  }
  if (screen.width >= 768) {
    if(document.location.host == 'm.thehashtag.news'){
      document.location = "https://www.thehashtag.news" + document.location.pathname;
    }
  }
  $(document).mouseup(function(e) {
    // console.log('mouseup');
  });
  $(window).load(function(e) {
    AddAdCodes();
    // console.log('Load Completed');
    $('.loader').hide();
  });
  /* default value for twitter widget */
  var start=0;
  var limit=8;
  var twitteritems;
  /* default value for twitter widget */
  $(document).ready(function() {
    $('#twitt-opt-reg .pager-load-more').hide();
    $('.twittloader-trob').hide();
    if ($('.twit-homepage').length > 0) {
      if (document.location.pathname == '/' || document.location.pathname == '/home' || document.location.pathname == '/home-sample') {
        $.getJSON('views/ajax?view_name=twitter_link&view_display_id=default', function(items) {
          twitteritems = items;
          GetTwitterCont();
          twttr = load_twitter(document, 'script', 'twitter-wjs');
          load_masonary(twttr);
        });
        $('#twitt-opt-reg .pager-load-more').show();
      }
      else if (document.location.pathname == '/breaking') {
        $.getJSON('views/ajax?view_name=twitter_link&view_display_id=breaking', function(items) {
          twitteritems = items;
          GetTwitterCont();
          twttr = load_twitter(document, 'script', 'twitter-wjs');
          load_masonary(twttr);
        });
        $('#twitt-opt-reg .pager-load-more').show();
      }
      else if (document.location.pathname == '/politics') {
        $.getJSON('views/ajax?view_name=twitter_link&view_display_id=politics', function(items) {
          twitteritems = items;
          GetTwitterCont();
          twttr = load_twitter(document, 'script', 'twitter-wjs');
          load_masonary(twttr);
        });
        $('#twitt-opt-reg .pager-load-more').show();
      }
      else if (document.location.pathname == '/sports') {
        $.getJSON('views/ajax?view_name=twitter_link&view_display_id=sports', function(items) {
          twitteritems = items;
          GetTwitterCont();
          twttr = load_twitter(document, 'script', 'twitter-wjs');
          load_masonary(twttr);
        });
        $('#twitt-opt-reg .pager-load-more').show();
      }
      else if (document.location.pathname == '/business') {
        $.getJSON('views/ajax?view_name=twitter_link&view_display_id=business', function(items) {
          twitteritems = items;
          GetTwitterCont();
          twttr = load_twitter(document, 'script', 'twitter-wjs');
          load_masonary(twttr);
        });
        $('#twitt-opt-reg .pager-load-more').show();
      }
      else if (document.location.pathname == '/editorial') {
        $.getJSON('views/ajax?view_name=twitter_link&view_display_id=editorial', function(items) {
          twitteritems = items;
          GetTwitterCont();
          twttr = load_twitter(document, 'script', 'twitter-wjs');
          load_masonary(twttr);
        });
        $('#twitt-opt-reg .pager-load-more').show();
      }
      else if (document.location.pathname == '/entertainment') {
        $.getJSON('views/ajax?view_name=twitter_link&view_display_id=entertainment', function(items) {
          twitteritems = items;
          GetTwitterCont();
          twttr = load_twitter(document, 'script', 'twitter-wjs');
          load_masonary(twttr);
        });
        $('#twitt-opt-reg .pager-load-more').show();
      }
      else if ((Drupal.settings.hashtag !== 'undefined' && Drupal.settings.hashtag != null) && Drupal.settings.hashtag.nodecategoryid !== 'undefined' && Drupal.settings.hashtag.nodecategoryid != null) {
        $.getJSON('views/ajax?view_name=twitter_link&view_display_id=featured_article&view_args='+Drupal.settings.hashtag.nodecategoryid, function(items) {
          twitteritems = items;
          GetTwitterCont();
          twttr = load_twitter(document, 'script', 'twitter-wjs');
          load_masonary(twttr);
        });
        $('#twitt-opt-reg .pager-load-more').show();
      }
    }
    $('#twitt-opt-reg .pager-load-more').click(function() {
       loadmore_twitter();
    });
    $('.gallery-prev-slider').hide();
    $('<div class="slider_count"></div>').insertAfter(".node_title");
    var totalItems = $('.node-type-add-news-article .owl-item').length;
    var currentIndex = $('div.active').index() + 1;
    $(".slider_count").html( currentIndex + "/" + totalItems);
    $(".node-type-add-news-article .owl-next").click(function(){
      var totalItems = $('.node-type-add-news-article .owl-item').length;
      var currentIndex = $('div.active').index() + 1;
      $(".slider_count").html( currentIndex + "/" + totalItems);
      googletag.pubads().refresh();
      if ($(".owl-item.active").prev().find('.right_side_wrapper .slider_external_link a').length > 0) {
        url = $(".owl-item.active").prev().find('.right_side_wrapper .slider_external_link a').attr('href');
        window.open(url, "_blank");
      }
    });
    $(".node-type-add-news-article .owl-prev").click(function(){
      var totalItems = $('.node-type-add-news-article .owl-item').length;
      var currentIndex = $('div.active').index() + 1;
        $(".slider_count").html( currentIndex + "/" + totalItems);
      googletag.pubads().refresh();
    });
    $(".node-type-add-news-article .owl-next").on('touchend', function() {
      var totalItems = $('.node-type-add-news-article .owl-item').length;
      var currentIndex = $('div.active').index() + 2;
      if (currentIndex > totalItems) {
        currentIndex = 1;
      }
      $(".slider_count").html( currentIndex + "/" + totalItems);
      googletag.pubads().refresh();
      $('html').animate({scrollTop : $(".owl-wrapper").offset().top},800);
      if ($(".owl-item.active").find('.right_side_wrapper .slider_external_link a').length > 0) {
        url = $(".owl-item.active").find('.right_side_wrapper .slider_external_link a').attr('href');
        window.open(url, "_blank");
      }
    });
    $(".node-type-add-news-article .owl-prev").on('touchend', function() {
      var totalItems = $('.node-type-add-news-article .owl-item').length;
      var currentIndex = $('div.active').index();
      if (currentIndex === 0) {
        currentIndex = totalItems;
      }
      $(".slider_count").html( currentIndex + "/" + totalItems);
      googletag.pubads().refresh();
      $('html').animate({scrollTop : $(".owl-wrapper").offset().top},800);
    }); 
    $(".node-type-add-news-article .gall_right").click(function(){
      $('.gallery-prev').fadeOut();
      $('.gallery-prev-slider').fadeIn();
    });
    $('.mobile .art-detail').each(function(e){
      var p_count = 0;
      $(this).find('p').each(function(e){
        p_count = p_count + 1;
      });
      half = Math.round(p_count/2);
      $("<div id='Incontent1'></div>").insertAfter($(this).find('p:eq('+ half+ ')'));
    });
    $('.ticker-hide').css('visibility', 'visible');
    if($('.masonry-item').length[0]) {
      window.setInterval(function() {
        if( collision($('.masonry-item'), $('.footer')) == true ){
          var margin = parseInt($('.footer').css('margin-top'), 10);
          $('.footer').css('margin-top', margin+50);
        }
      }, 100);
    }
    $('.view-article-listing .masonry-item').each(function(e){
      $(this).prependTo('.view-hashtags-news-listing .view-content');
    });
      // match width of buttons
      var btnwidth = $('.owl-prev').width();
      $('.mobile-right').css('width', btnwidth + 5);
      $('.mobile-right .news-addtoany a').css('width', btnwidth + 5);
    const mq = window.matchMedia("(max-width: 768px)");
    if (mq.matches) {
      $('body').addClass('mobile');
    // Article Read It Now
      $('a.view-more-link').click(function(e) {
        e.preventDefault();
        $(this).text($(this).text() === 'View Less' ? 'Read It Now' : 'View Less');
        $(this).parent().siblings('.col-exp').toggleClass('short-text long-text');
      });
    } else {
      $('body').addClass('no-mobile');
    // Article Read It Now
      $('a.view-more-link').click(function(e) {
        e.preventDefault();
        $(this).text($(this).text() === 'View Less' ? 'Read It Now' : 'View Less');
        $(this).parent().siblings('.col-exp').toggleClass('short-text long-text');
        $.scrollTo('.short-text', 'slow', {
          'offset': 0
        });
        console.log('Masonry 1 Called');
        $(this).parents('.view-content.masonry-processed').masonry({
          itemSelector: '.masonry-item',
          columnWidth: '.col-md-3',
          percentPosition: true
        });

      });
    }
    // Tweeter Buttun Height
    var tweetheight = $('.mobile-share').height();
    $('.mobile .art-share a').css('height', tweetheight-10 + 'px')
    // login Layer Click
    $('.mobile-menu-close').click(function(){
      $(this).parents('.responsive-menus-simple').siblings('span.toggler').trigger('click');
    });
    // Trigre owl Caroucel Controllers
    $('a.next').click(function() {
      $(this).parents('.owl-wrapper-outer').siblings('.owl-controls').find('.owl-prev').trigger('click');
    });
    // Previous Page
    $('a.comment-back.go-back').click(function() {
      window.history.back();
    });
    var p_count = 0;
    $('.node-type-add-news-article .field-name-body .field-item').find('p').each(function(e){
     p_count = p_count + 1;
    });
    var half = Math.round(p_count/2);
    $(".field-name-field-featured-article-ad-code").insertAfter($('.node-type-add-news-article .field-name-body .field-item').find('p:eq('+ half+ ')'));
    // Form Resize
    var mdlheight = $(window).height();
    // console.log(mdlheight);
    $('.modal-scroll #modal-content').css('max-height', mdlheight - 100);
    $(window).resize(function() {
      if (screen.width < 768) {
        if(document.location.host == 'thehashtag.news' || document.location.host == 'www.thehashtag.news' ){
          document.location = "https://m.thehashtag.news" + document.location.pathname;
        }
      }
      if (screen.width >= 768) {
        if(document.location.host == 'm.thehashtag.news'){
          document.location = "https://www.thehashtag.news" + document.location.pathname;
        }
      }
      var mdlheight = $(window).height();
      // console.log(mdlheight);
      $('.modal-scroll #modal-content').css('max-height', mdlheight - 50);
      // Tweeter Buttun Height
      // match width of buttons
      var btnwidth = $('.owl-prev').width();
      $('.mobile-right').css('width', btnwidth + 5);
      $('.mobile-right .news-addtoany a').css('width', btnwidth + 5);
    });
    $('#feed-node-form, #add-news-article-node-form').addClass('col-md-8 col-md-offset-2')
    $('#add-news-article-node-form .form-group').addClass('col-md-12');
    $('#edit-feeds .panel-title.fieldset-legend').text('Keyhole API Endpoint');
    $('#autocomplete-deluxe-input, #edit-field-news-hashtags-tid-value-field, #edit-field-news-hashtags-tid').each(function(e){
      $(this).on('focus keyup mouseup', function(e) {
        var text = $(this).val();
        if (text.charAt(0) != '#') {
          $(this).val('#');
        }
      });
    });
    $('.check-link').click(function(e) {
      e.preventDefault();
      var path = $('#edit-field-news-link-und-0-url');
      if (!ValidURL(path.val())) {
        alert('Please enter a valid url. E.g http://example.com.');
        if ($(this).find('i').hasClass('fa-times')) {} else if ($(this).find('i').hasClass('fa-check')) {
          $(this).find('i').removeClass('fa-check').addClass('fa-times');
        } else {
          $(this).append('<i class="fa fa-times" aria-hidden="true"></i>');
          $(this).find('.fa').css('padding-left', '10px');
        }
      } else {
        $.ajax({
          url: path,
          crossDomain: true,
          complete: function(xhr, statusText) {
            if (xhr.status == 200) {
              if ($('.check-link').find('i').hasClass('fa-check')) {} else if ($('.check-link').find('i').hasClass('fa-times')) {
                $('.check-link').find('i').removeClass('fa-times').addClass('fa-check');
              } else {
                $('.check-link').append('<i class="fa fa-check" aria-hidden="true"></i>');
                $('.check-link').find('.fa').css('padding-left', '10px');
              }
            }
          },
          error: function(xhr, statusText) {
            alert('Please enter a valid url. E.g http://example.com.');
          },
        })
      }
    });
    // Tooltip
    $('#edit-field-news-link-und-0-url').attr("data-toggle", "tooltip").attr("data-html", "true").attr("maxlength", "1024").attr("title", '- Enter the link for your article then click “Check Link”');
    // Sticky Nav
    $(function() {
      $(window).scroll(function() {
        function addStickyMenu() {
          if(!($("body, section#block-views-hashtags-listing-popular-tags, .navigation-div, .sliding-bar .block-views .views-exposed-form").hasClass('sticky'))) {
            $("body, section#block-views-hashtags-listing-popular-tags, .navigation-div, .sliding-bar .block-views .views-exposed-form").addClass('sticky');
          }
        }
        function removeStickyMenu() {
          if($("body, section#block-views-hashtags-listing-popular-tags, .navigation-div, .sliding-bar .block-views .views-exposed-form").hasClass('sticky')) {
            $("body, section#block-views-hashtags-listing-popular-tags, .navigation-div, .sliding-bar .block-views .views-exposed-form").removeClass('sticky');
          }
        }
        function addStickyCat() {
          if(!($("#block-menu-menu-news-category, body:not(body.page-node-23)").hasClass('stick'))) {
            $("#block-menu-menu-news-category, body:not(body.page-node-23)").addClass('stick');
          }
        }
        function removeStickyCat() {
          if($("#block-menu-menu-news-category, body:not(body.page-node-23)").hasClass('stick')) {
            $("#block-menu-menu-news-category, body:not(body.page-node-23)").removeClass('stick');
          }
        }
        var winWidth = $(window).width();
        var winTop = $(window).scrollTop();
        if (winWidth >= 1199) {
          if (winTop >= 95) {
            addStickyMenu();
          } else {
            removeStickyMenu();
          }
          if (winTop >= 250) {
            addStickyCat();
          } else {
            removeStickyCat();
          }
        } else if (winWidth >= 1023) {
          if (winTop >= 80) {
            addStickyMenu();
          } else {
            removeStickyMenu();
          }
          if (winTop >= 242) {
            addStickyCat();
          } else {
            removeStickyCat();
          }
        } else if (winWidth >= 989) {
          if (winTop >= 80) {
            addStickyMenu();
          } else {
            removeStickyMenu();
          }
          if (winTop >= 242) {
            addStickyCat();
          } else {
            removeStickyCat();
          }
        } else if (winWidth >= 767) {
          if (winTop >= 70) {
            addStickyMenu();
          } else {
            removeStickyMenu();
          }
          if (winTop >= 225) {
            addStickyCat();
          } else {
            removeStickyCat();
          }
        } else if (winWidth <=768 && winWidth >=480) {
          if (winTop >= 145) {
            addStickyMenu();
          } else {
            removeStickyMenu();
          }
        } else if (winWidth <=479 && winWidth >=320) {
          if (winTop >= 130) {
            addStickyMenu();
          } else {
            removeStickyMenu();
          }
        }else if (winWidth <=319) {
          if (winTop >= 120) {
            addStickyMenu();
          } else {
            removeStickyMenu();
          }
        }
      });
    });
    $('form.node-hash_tag_news-form').on('submit', function() {
        var path = $('#edit-field-news-link-und-0-url');
        if (!ValidURL(path.val())) {
          alert('Please enter a valid url. E.g http://example.com.');
          return;
        }
      })
      //Replace Share Text
    //palceholder
    $('#edit-field-news-link-und-0-url').attr('placeholder', $.parseHTML("&#xf0c1;")[0].data + '  ' + 'Enter the article URL here...').css({
      "font-size": "17px"
    });
    $('#edit-field-news-caption-und-0-value').attr('placeholder', 'Tag lines goes here...').css({
      "font-size": "17px"
    });
    // Close Login Form By Clicking Any Where
    Drupal.behaviors.ctools_backdrop_close = {
        attach: function(context, settings) {
          $('#modalBackdrop').once('ctools_backdrop_close', function() {
            $(this).click(function() {
              Drupal.CTools.Modal.dismiss();
            });
          });
        }
      }
      // $('#edit-field-news-markup').parent().next().find('a').addClass('checklink');
    $('a.a2a_dd.addtoany_share_save').addClass('fa fa-share-alt');
    $('.match-height').matchHeight();
    $('.menu-image').after().append('<p class="grad-back"></p>');
    $('#block-block-16').find('.recentcomment').attr('id', 'style');
    // $('a.sidebar-arrow').click(function(e) {
    //   $('a.sidebar-arrow i.fa').toggleClass('fa-chevron-left');
    //   var parent = $(this).closest('aside');
    //   parent.toggleClass("open");
    //   var parent1 = $(this).parents().find('.hashtag-list');
    //   e.preventDefault();
    // });
    $('a.sidebar-arrow').click(function(){
      $(this).parents('.header-top').siblings('.responsive-menus').find('.toggler').trigger('click');
    })
    $('div#rm-removed').on('click', function(e) {
      $(this).siblings('.toggler').trigger('click');
    });
    $("div#rm-removed").find('*').click(function(e) {
      e.stopPropagation();
    });
    // $('span.toggler').click(function(){
    //   $(this).find('a.sidebar-arrow').toggle();
    // })
    placeholders();

    function placeholders() {
      $('#edit-field-news-hashtags #autocomplete-deluxe-input').each(function() {
        $(this).attr('placeholder', '# Add new hashtag here!');
      });
      $('.sliding-bar .views-exposed-form #autocomplete-deluxe-input, #edit-field-news-hashtags-tid').each(function() {
        $(this).attr('placeholder', '# Search');
      });
    }
    // the hashtag news text pop up
    var modal = document.getElementById('myModal');
    var btn = document.getElementById("myBtn");
    var span = document.getElementsByClassName("close")[0];
    if (btn) {
      btn.onclick = function() {
        modal.style.display = "block";
      }
    }
    if (span) {
      span.onclick = function() {
        modal.style.display = "none";
      }
    }
    if (window) {
      window.onclick = function(event) {
        if (event.target == modal) {
          modal.style.display = "none";
        }
      }
    }
    // end pop up
    var setHeight = $(window).height();
    $('.region.region-mobile-menu').css('height', setHeight + 'px');
    $("div.modal-forms-modal-content .popups-container").css('height', setHeight);
    $('#block-block-11 i').click(function(e) {
      $('#block-block-11').fadeOut();
    });
    $('.form-item-edit-field-news-category-tid-12 a').trigger('click');
    $('form#views-exposed-form-hashtags-news-listing-block div#edit-field-news-category-tid-wrapper .form-item-edit-field-news-category-tid-12 a').prepend('<i class="fa fa-clock-o"></i>');
    $('form#views-exposed-form-hashtags-news-listing-block div#edit-field-news-category-tid-wrapper .form-item-edit-field-news-category-tid-11 a').prepend('<i class="fa fa-star"></i>');
    $('form#views-exposed-form-hashtags-news-listing-block div#edit-field-news-category-tid-wrapper .form-item-edit-field-news-category-tid-13 a').prepend('<i class="fa fa-snapchat-ghost"></i>');
    $('form#views-exposed-form-hashtags-news-listing-block div#edit-field-news-category-tid-wrapper .form-item-edit-field-news-category-tid-14 a').prepend('<i class="fa fa-play"></i>');
    $('form#views-exposed-form-hashtags-news-listing-block div#edit-field-news-category-tid-wrapper .form-item-edit-field-news-category-tid-15 a').prepend('<i class="fa fa-user"></i>');
    $('.navbar-menu-bottom .block.block-menu ul.menu.nav li.leaf:nth-child(1) a').prepend('<i class="fa fa-home"></i>');
    $('.navbar-menu-bottom .block.block-menu ul.menu.nav li.leaf:nth-child(2) a').prepend('<i class="fa fa-commenting"></i>');
    $('.navbar-menu-bottom .block.block-menu ul.menu.nav li.leaf:nth-child(3) a').prepend('<i class="fa fa-car"></i>');
    $('.navbar-menu-bottom .block.block-menu ul.menu.nav li.leaf:nth-child(4) a').prepend('<i class="fa fa-cog"></i>');
    $('#edit-field-news-category-und option[value="_none"]').text('Select Category');
    $('.node.node-hash-tag-news.view-mode-full .ds-left span.a2a_kit.a2a_kit_size_32.a2a_target.addtoany_list a').addClass('fa fa-share-alt');
    $('.node.node-hash-tag-news.view-mode-full .ds-left .field.field-name-node-link.field-type-ds a').addClass('fa fa-eye');
    $('.node.node-hash-tag-news.view-mode-full .ds-left .field.field-name-field-news-comments .field-items .field-item.even').addClass('fa fa-comment');
    $('.node.node-hash-tag-news.view-mode-full .ds-right .field.field-name-field-news-retweets.field-type-number-decimal .field-item.even').addClass('fa fa-retweet');
    $('.node.node-hash-tag-news.view-mode-full .ds-right .field.field-name-field-news-favorites.field-type-number-decimal .field-item.even').addClass('fa fa-heart');
    $('form#hash-tag-news-node-form input#edit-field-news-url-und-0-url').attr("placeholder", $.parseHTML("&#xf0c1;")[0].data + " Enter the article URL here...").css('font-size', '17px');
    $('input#edit-field-news-source-und').attr("placeholder", $.parseHTML("&#xf0e7;")[0].data + " Website Name (Source)").css('font-size', '17px');
    $('textarea#edit-field-news-body-und-0-value').attr("placeholder", "Tag line goes here").css('font-size', '17px');
    $('form#hash-tag-news-node-form .autocomplete-deluxe-container.autocomplete-deluxe-multiple').prepend('<p>ASSIGN HASHTAG <span>(add as many as you like!)</spane></p>');
    $('footer.footer .form-border input#edit-mergevars-email').attr("placeholder", "Your email adress").css('font-size', '17px');
    // console.log('Document Ready');
    $('.row-ad:last .banner-ad').bind('DOMNodeInserted DOMNodeRemoved DOMSubtreeModified', function(event) {
      if (event.type == 'DOMNodeInserted') {
        $(this).parents('.view-content.masonry-processed').masonry({
          itemSelector: '.masonry-item',
          columnWidth: '.col-md-3',
          percentPosition: true
        });
      } else if (event.type == 'DOMSubtreeModified') {
      } else if (event.type == 'DOMNodeRemoved') {
      } else {}
    });
    $('.mobile.not-front .row-ad:last, .no-mobile .row-ad:last').prev().find('.tweet-listing').bind('DOMNodeInserted DOMNodeRemoved DOMSubtreeModified', function(event) {
      if (event.type == 'DOMNodeInserted') {
      } else if (event.type == 'DOMSubtreeModified') {
      } else if (event.type == 'DOMNodeRemoved') {
        console.log('Masonry Oembed Called');
        $(this).parents('.view-content.masonry-processed').masonry({
          itemSelector: '.masonry-item',
          columnWidth: '.col-md-3',
          percentPosition: true
        });
        // $('.pager.pager-load-more a').trigger('click');
      } else {}
    });
    // border-bottom body field
    setTimeout(
      function(){
        var body_check = $('.art-wrap .field-name-body');
        if (body_check.length) {
          var cur_bodysize = $('.art-wrap .field-name-body').offset().top + $('.art-wrap .field-name-body').height();
          var media_lar_size = 0;
          var media_check = $('.art-wrap .field-name-body .media-element-container');
          if (media_check.length) {
            $('.art-wrap .field-name-body .media-element-container').each(function() {
              var tmp = $(this).offset().top + $(this).height();
              if (tmp > media_lar_size) {
                media_lar_size = tmp;
              }
            });
          }
          if (media_lar_size > 0 && media_lar_size > cur_bodysize) {
            $('.art-wrap .field-name-body').height((10 + media_lar_size) - $('.art-wrap .field-name-body').offset().top); 
          }
        }
      }, 10000
    );  
  });
  $(document).ajaxComplete(function(e) {
    $('span.close').click(function(){
      $(this).parents('.modal').trigger('click');
    });
    // Form Resize
    var mdlheight = $(window).height();
    // console.log(mdlheight);
    $('.modal-scroll #modal-content').css('max-height', mdlheight - 100);
    $(window).resize(function() {
      var mdlheight = $(window).height();
      // console.log(mdlheight);
      $('.modal-scroll #modal-content').css('max-height', mdlheight - 50);
      // match width of buttons
      var btnwidth = $('.owl-prev').width();
      $('.mobile-right .news-viewnews').css('width', '100%');
    });
    var setHeight = $(window).height();
    $("div.modal-forms-modal-content .popups-container").css('height', setHeight);
    $(".login-footer").css('top', setHeight - 45);
    $(".register-footer h5:last-child").css('top', setHeight - 30);
    // Add Icons
    $('.node.node-hash-tag-news.view-mode-full .ds-left span.a2a_kit.a2a_kit_size_32.a2a_target.addtoany_list a').addClass('fa fa-share-alt');
    $('.node.node-hash-tag-news.view-mode-full .ds-left .field.field-name-node-link.field-type-ds a').addClass('fa fa-eye');
    $('.node.node-hash-tag-news.view-mode-full .ds-left .field.field-name-field-news-comments .field-items .field-item.even').addClass('fa fa-comment');
    $('.node.node-hash-tag-news.view-mode-full .ds-right .field.field-name-field-news-retweets.field-type-number-decimal .field-item.even').addClass('fa fa-retweet');
    $('.node.node-hash-tag-news.view-mode-full .ds-right .field.field-name-field-news-favorites.field-type-number-decimal .field-item.even').addClass('fa fa-heart');
    // Signin / Signup Place Holders
    $('form#user-login .form-item-name input#edit-name').attr("placeholder", $.parseHTML(" ")[0].data + "Email").css('font-size', '17px');
    $('.form-item-name input#edit-name--2').attr("placeholder", $.parseHTML(" ")[0].data + "Email").css('font-size', '17px');
    $('form#user-login .form-item-pass input#edit-pass').attr("placeholder", $.parseHTML(" ")[0].data + "Password").css('font-size', '17px');
    $('.form-item-pass input#edit-pass--2').attr("placeholder", $.parseHTML(" ")[0].data + "Password").css('font-size', '17px');
    $('form#user-register-form .form-item-name input#edit-name--2').attr("placeholder", $.parseHTML(" ")[0].data + "User Name").css('font-size', '17px');
    $('form#user-register-form .form-item-name input#edit-name--3').attr("placeholder", $.parseHTML(" ")[0].data + "User Name").css('font-size', '17px');
    $('form#user-register-form .form-item-mail input#edit-mail--2').attr("placeholder", $.parseHTML(" ")[0].data + "Email").css('font-size', '17px');
    $('form#user-register-form .form-item-pass-pass1 input#edit-pass-pass1--2').attr("placeholder", $.parseHTML(" ")[0].data + "Password").css('font-size', '17px');
    $('form#user-register-form .form-item-pass-pass2 input#edit-pass-pass2--2').attr("placeholder", $.parseHTML(" ")[0].data + "Re-Type Password").css('font-size', '17px');
    $('form#user-pass .form-item-name input#edit-name--3').attr("placeholder", $.parseHTML(" ")[0].data + "Email").css('font-size', '17px');
    $('div.modal-forms-modal-content .popups-close').addClass('fa fa-times').text('');
    $('.register-footer').click(function() {
      $(this).hide();
      $('.modal-default, .backdrop-default').hide();
    });
    $('.backdrop-default').click(function() {
      $(this).hide();
      $('.modal-default, .backdrop-default').hide();
    });
    $('.row-ad:last .banner-ad').bind('DOMNodeInserted DOMNodeRemoved DOMSubtreeModified', function(event) {
      if (event.type == 'DOMNodeInserted') {
        $(this).parents('.view-content.masonry-processed').masonry({
          itemSelector: '.masonry-item',
          columnWidth: '.col-md-3',
          percentPosition: true
        });
      } else if (event.type == 'DOMSubtreeModified') {
      } else if (event.type == 'DOMNodeRemoved') {
      } else {}
    });
    $('.mobile.not-front .row-ad:last, .no-mobile .row-ad:last').prev().find('.tweet-listing').bind('DOMNodeInserted DOMNodeRemoved DOMSubtreeModified', function(event) {
      if (event.type == 'DOMNodeInserted') {
      } else if (event.type == 'DOMSubtreeModified') {
      } else if (event.type == 'DOMNodeRemoved') {
        console.log('Masonry Ajax Oembed Called');
        $(this).parents('.view-content.masonry-processed').masonry({
          itemSelector: '.masonry-item',
          columnWidth: '.col-md-3',
          percentPosition: true
        });

      } else {}
    });
    // console.log('Ajax Completed');
  });
  var AddAdCodes = function AddAdCodes(e) {
    var TopLeaderboard = document.createElement('script');
    TopLeaderboard.type = 'text/javascript';
    TopLeaderboard.async = true;
    TopLeaderboard.innerHTML="googletag.cmd.push(function(){googletag.display('TopLeaderboard');});";
    $('#TopLeaderboard').each(function(e){
      if(!$.trim($(this).html()).length){
        $(this).append(TopLeaderboard);
      }
    });
    var Sidebar1 = document.createElement('script');
    Sidebar1.type = 'text/javascript';
    Sidebar1.async = true;
    Sidebar1.innerHTML="googletag.cmd.push(function(){googletag.display('Sidebar1');});";
    $('#Sidebar1').each(function(e){
      if(!$.trim($(this).html()).length){
        $(this).append(Sidebar1);
      }
    });
    var Sidebar2 = document.createElement('script');
    Sidebar2.type = 'text/javascript';
    Sidebar2.async = true;
    Sidebar2.innerHTML="googletag.cmd.push(function(){googletag.display('Sidebar2');});";
    $('#Sidebar2').each(function(e){
      if(!$.trim($(this).html()).length){
        $(this).append(Sidebar2);
      }
    });
    var Incontent1 = document.createElement('script');
    Incontent1.type = 'text/javascript';
    Incontent1.async = true;
    Incontent1.innerHTML="googletag.cmd.push(function(){googletag.display('Incontent1');});";
    $('#Incontent1').each(function(e){
      if(!$.trim($(this).html()).length){
        $(this).append(Incontent1);
      }
    });
    var Incontent2 = document.createElement('script');
    Incontent2.type = 'text/javascript';
    Incontent2.async = true;
    Incontent2.innerHTML="googletag.cmd.push(function(){googletag.display('Incontent2');});";
    $('#Incontent2').each(function(e){
      if(!$.trim($(this).html()).length){
        $(this).append(Incontent2);
      }
    });
    var Incontent3 = document.createElement('script');
    Incontent3.type = 'text/javascript';
    Incontent3.async = true;
    Incontent3.innerHTML="googletag.cmd.push(function(){googletag.display('Incontent3');});";
    $('#Incontent3').each(function(e){
      if(!$.trim($(this).html()).length){
        $(this).append(Incontent3);
      }
    });
    var Incontent4 = document.createElement('script');
    Incontent4.type = 'text/javascript';
    Incontent4.async = true;
    Incontent4.innerHTML="googletag.cmd.push(function(){googletag.display('Incontent4');});";
    $('#Incontent4').each(function(e){
      if(!$.trim($(this).html()).length){
        $(this).append(Incontent4);
      }
    });
    var Incontent5 = document.createElement('script');
    Incontent5.type = 'text/javascript';
    Incontent5.async = true;
    Incontent5.innerHTML="googletag.cmd.push(function(){googletag.display('Incontent5');});";
    $('#Incontent5').each(function(e){
      if(!$.trim($(this).html()).length){
        $(this).append(Incontent5);
      }
    });
    var Incontent6 = document.createElement('script');
    Incontent6.type = 'text/javascript';
    Incontent6.async = true;
    Incontent6.innerHTML="googletag.cmd.push(function(){googletag.display('Incontent6');});";
    $('#Incontent6').each(function(e){
      if(!$.trim($(this).html()).length){
        $(this).append(Incontent6);
      }
    });
    var Leaderboard2 = document.createElement('script');
    Leaderboard2.type = 'text/javascript';
    Leaderboard2.async = true;
    Leaderboard2.innerHTML="googletag.cmd.push(function(){googletag.display('Leaderboard2');});";
    $('#Leaderboard2').each(function(e){
      if(!$.trim($(this).html()).length){
        $(this).append(Leaderboard2);
      }
    });
    var rcjsload_7158e5 = document.createElement('script');
    rcjsload_7158e5.type = 'text/javascript';
    rcjsload_7158e5.async = true;
    rcjsload_7158e5.innerHTML='(function() {var referer="";try{if(referer=document.referrer,"undefined"==typeof referer)throw"undefined"}catch(exception){referer=document.location.href,(""==referer||"undefined"==typeof referer)&&(referer=document.URL)}referer=referer.substr(0,700);var rcel = document.createElement("script");rcel.id = "rc_" + Math.floor(Math.random() * 1000);rcel.type = "text/javascript";rcel.src = "https://trends.revcontent.com/serve.js.php?w=83848&t="+rcel.id+"&c="+(new Date()).getTime()+"&width="+(window.outerWidth || document.documentElement.clientWidth)+"&referer="+referer;rcel.async = true;var rcds = document.getElementById("rcjsload_7158e5"); rcds.appendChild(rcel);})();';
    $('#rcjsload_7158e5').each(function(e){
      $(this).append(rcjsload_7158e5);
    });
    var rcjsload_9dcc9b = document.createElement('script');
    rcjsload_9dcc9b.type = 'text/javascript';
    rcjsload_9dcc9b.async = true;
    rcjsload_9dcc9b.innerHTML='(function() {var referer="";try{if(referer=document.referrer,"undefined"==typeof referer)throw"undefined"}catch(exception){referer=document.location.href,(""==referer||"undefined"==typeof referer)&&(referer=document.URL)}referer=referer.substr(0,700);var rcel = document.createElement("script");rcel.id = "rc_" + Math.floor(Math.random() * 1000);rcel.type = "text/javascript";rcel.src = "https://trends.revcontent.com/serve.js.php?w=83849&t="+rcel.id+"&c="+(new Date()).getTime()+"&width="+(window.outerWidth || document.documentElement.clientWidth)+"&referer="+referer;rcel.async = true;var rcds = document.getElementById("rcjsload_9dcc9b"); rcds.appendChild(rcel);})();';
    $('#rcjsload_9dcc9b').each(function(e){
      $(this).append(rcjsload_9dcc9b);
    });
    var rcjsload_7606ad = document.createElement('script');
    rcjsload_7606ad.type = 'text/javascript';
    rcjsload_7606ad.async = true;
    rcjsload_7606ad.innerHTML='(function() {var referer="";try{if(referer=document.referrer,"undefined"==typeof referer)throw"undefined"}catch(exception){referer=document.location.href,(""==referer||"undefined"==typeof referer)&&(referer=document.URL)}referer=referer.substr(0,700);var rcel = document.createElement("script");rcel.id = "rc_" + Math.floor(Math.random() * 1000);rcel.type = "text/javascript";rcel.src = "https://trends.revcontent.com/serve.js.php?w=83842&t="+rcel.id+"&c="+(new Date()).getTime()+"&width="+(window.outerWidth || document.documentElement.clientWidth)+"&referer="+referer;rcel.async = true;var rcds = document.getElementById("rcjsload_7606ad"); rcds.appendChild(rcel);})();';
    $('#rcjsload_7606ad').each(function(e){
      $(this).append(rcjsload_7606ad);
    });
    var rcjsload_1a0ad2 = document.createElement('script');
    rcjsload_1a0ad2.type = 'text/javascript';
    rcjsload_1a0ad2.async = true;
    rcjsload_1a0ad2.innerHTML='(function() {var referer="";try{if(referer=document.referrer,"undefined"==typeof referer)throw"undefined"}catch(exception){referer=document.location.href,(""==referer||"undefined"==typeof referer)&&(referer=document.URL)}referer=referer.substr(0,700);var rcel = document.createElement("script");rcel.id = "rc_" + Math.floor(Math.random() * 1000);rcel.type = "text/javascript";rcel.src = "https://trends.revcontent.com/serve.js.php?w=83512&t="+rcel.id+"&c="+(new Date()).getTime()+"&width="+(window.outerWidth || document.documentElement.clientWidth)+"&referer="+referer;rcel.async = true;var rcds = document.getElementById("rcjsload_1a0ad2"); rcds.appendChild(rcel);})();';
    $('#rcjsload_1a0ad2').each(function(e){
      $(this).append(rcjsload_1a0ad2);
    });
  }
  var ValidURL = function ValidURL(str) {
    var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    if (!regex.test(str)) {
      // alert("Please enter valid URL.");
      return false;
    } else {
      return true;
    }
  }

  var collision = function collision($div1, $div2) {
    var x1 = $div1.offset().left;
    var y1 = $div1.offset().top;
    var

    h1 = $div1.outerHeight(true);
    var w1 = $div1.outerWidth(true);
    var b1 = y1 + h1;
    var r1 = x1 + w1;
    var x2 = $div2.offset().left;
    var w2 = $div2.outerWidth(true);
    var y2 = $div2.offset().top;
    var h2 = $div2.outerHeight(true);
    var b2 = y2 + h2;
    var r2 = x2 + w2;

    if (b1 < y2 || y1 > b2 || r1 < x2 || x1 > r2) return false;
    return true;
  }
  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;
    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');
      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  };
  /* Twitter widget dynamic display */
  var GetTwitterCont = function GetTwitterCont() {
   // js.src = "//npmcdn.com/masonry-layout@4.0/dist/masonry.pkgd.js";
    if(twitteritems !== undefined) {
      var limits = start + limit;
      var data = twitteritems.slice(start, limits);
      for(index = 0; index < data.length; index++) {
        var twittcontent = "<div class='masonry-item col-lg-3 col-md-3 col-sm-4 col-xs-12 grid-item'><div class='views-field views-field-nothing'><div class='row no-gutter'><div class='col-md-8 col-sm-8 col-xs-8 no-gutter'><div class='news-accountno-category col-md-12 col-sm-12 col-xs-12 no-gutter'>"+data[index].field_news_category+"</div><div class='news-created-date col-md-12 col-sm-12 col-xs-12 no-gutter'><span class='ficon'><i class='fa fa-clock-o'> </i> </span><span> "+data[index].field_news_fetch_date+"</span></div></div><div class='news-comments-count col-md-4 col-sm-4 col-xs-4 no-gutter'><a href="+data[index].path+" target='_blank'><span class='ficon'><i class='fa fa-comment-o'></i></span><span class='disqus-comment-count' data-disqus-url="+data[index].path+"></span></a></div><div class='col-md-12 col-sm-12 col-xs-12 no-gutter tweet-listing'><blockquote class='twitter-tweet'><a href="+data[index].field_news_link+"></a></blockquote></div><div class='news-hashtags col-md-12 col-sm-12 col-xs-12  no-gutter'>"+data[index].field_news_hashtags+"</div><div class='news-viewnews col-md-12 col-sm-12 col-xs-12 no-gutter match-height'><a href="+data[index].field_news_link+" target='_blank'>View Article <i class='fa fa-share-square'></i></a></div><div class='col-md-12 col-sm-12 col-xs-12 no-gutter'><div class='social-icons-trigger col-md-12 col-sm-12 col-xs-12 no-gutter'><div class='a2a_kit my_centered_buttons' data-a2a-url="+data[index].field_news_link+" data-a2a-title="+data[index].title+"><a class='a2a_button_twitter'>Tweet it Now ("+data[index].field_news_retweets+") <i class='fa fa-twitter'></i></a></div><div class='social-icons col-md-12 col-sm-12 col-xs-12 no-gutter'><div class='a2a_kit a2a_kit_size_32 a2a_default_style my_centered_buttons share-this' data-a2a-url="+data[index].field_news_link+" data-a2a-title="+data[index].title+"><a class='a2a_button_facebook'></a><a class='a2a_button_twitter'></a><a class='a2a_button_google_plus'></a><a class='a2a_button_pinterest'></a></div></div></div></div></div></div></div>";
        if (start == 0 && index == 1) {
          twittcontent += "<div class='masonry-item col-lg-3 col-md-3 col-sm-4 col-xs-12 grid-item masonry-ad-row'><div class='views-field views-field-nothing'><div id='Incontent3' class='banner-ad'></div></div></div>";
        }
        else if (start == 0 && index == 3 && (typeof Drupal.settings.hashtag === 'undefined')) {
          twittcontent += "<div class='masonry-item col-lg-3 col-md-3 col-sm-4 col-xs-12 grid-item masonry-ad-row'><div class='views-field views-field-nothing'><div id='Incontent4' class='banner-ad'></div></div></div>";
        }
        else if (start == 0 && index == 5) {
          twittcontent += "<div class='masonry-item col-lg-3 col-md-3 col-sm-4 col-xs-12 grid-item masonry-ad-row'><div class='views-field views-field-nothing'><div id='Incontent5' class='banner-ad'></div></div></div>";
        }
        else if (start == 0 && index == 7) {
          twittcontent += "<div class='masonry-item col-lg-3 col-md-3 col-sm-4 col-xs-12 grid-item masonry-ad-row'><div class='views-field views-field-nothing'><div id='Incontent6' class='banner-ad'></div></div></div>";
        }
        $("#twitt-opt-reg .twit-homepage").append(twittcontent);
      }
      start = start + limit;
      a2a_config.target = '.share-this';
      a2a.init('page');
      // When widget is ready, run masonry
      //$('.grid').masonry({
      //  itemSelector : '.grid-item',
      //  columnWidth : '.col-md-3',
      //  percentPosition: true
      //});
    }
    if(limits >= twitteritems.length){
      $("#twitt-opt-reg .pager-load-more").fadeOut(3000);
    }
  };
  /* load twitter script */
  function load_twitter(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
    js = d.createElement(s);
    js.id = id;
    js.src = "https://platform.twitter.com/widgets.js";
    fjs.parentNode.insertBefore(js, fjs);
    t._e = [];
    t.ready = function(f) {
      t._e.push(f);
    };
    return t; 
  }
  /* load masonary script */
  function load_masonary(twttr){
    twttr.ready(function (twttr) {
      twttr.events.bind('loaded', function (event) {
        $('.grid').masonry({
          itemSelector : '.grid-item',
          columnWidth : '.col-md-3',
          percentPosition: true
        });
      });
    });
  }
  /* reload masonary script */
  function reload_masonary(twttr){
    twttr.events.bind('loaded', function (event) {
      $('.grid').masonry( 'reloadItems' );
      $('.grid').masonry( 'layout' );
    });
  }
  /* Pull twitter when load more */
  function loadmore_twitter(){
    $('.twittloader-trob').show();
    GetTwitterCont();
    twttr = load_twitter(document, 'script', 'twitter-wjs');
    reload_masonary(twttr);
    $('.twittloader-trob').hide(2250);
  }
})(jQuery);
