<?php

/**
 * @file
 * Display Suite Hashtag 90_10 Layout configuration.
 */

function ds_hashtag_90_10_layout() {
  return array(
    'label' => t('Hashtag 90_10 Layout'),
    'regions' => array(
      'top' => t('Top'),
      'left' => t('Left'),
      'right' => t('Right'),
      'bottom' => t('Bottom'),
    ),
    // Uncomment if you want to include a CSS file for this layout (hashtag_90_10_layout.css)
    'css' => TRUE,
    // Uncomment if you want to include a preview for this layout (hashtag_90_10_layout.png)
    // 'image' => TRUE,
  );
}
