<?php
/**
 * @file
 * featured_article.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function featured_article_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-add_news_article-body'.
  $field_instances['node-add_news_article-body'] = array(
    'bundle' => 'add_news_article',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'masonry' => FALSE,
          'masonry_animated' => 0,
          'masonry_animation_duration' => 500,
          'masonry_column_width' => '',
          'masonry_column_width_units' => 'px',
          'masonry_fit_width' => 0,
          'masonry_gutter_width' => 0,
          'masonry_images_first' => 1,
          'masonry_resizable' => 1,
          'masonry_rtl' => 0,
          'masonry_stamp_selector' => '',
        ),
        'type' => 'text_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'masonry' => FALSE,
          'masonry_animated' => 0,
          'masonry_animation_duration' => 500,
          'masonry_column_width' => '',
          'masonry_column_width_units' => 'px',
          'masonry_fit_width' => 0,
          'masonry_gutter_width' => 0,
          'masonry_images_first' => 1,
          'masonry_resizable' => 1,
          'masonry_rtl' => 0,
          'masonry_stamp_selector' => '',
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => FALSE,
    'settings' => array(
      'display_summary' => TRUE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-add_news_article-field_external_content'.
  $field_instances['node-add_news_article-field_external_content'] = array(
    'bundle' => 'add_news_article',
    'default_value' => array(
      0 => array(
        'value' => '<div id="rcjsload_7606ad"></div>',
        'format' => 'php_code',
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'masonry' => FALSE,
          'masonry_animated' => 0,
          'masonry_animation_duration' => 500,
          'masonry_column_width' => '',
          'masonry_column_width_units' => 'px',
          'masonry_fit_width' => 0,
          'masonry_gutter_width' => 0,
          'masonry_images_first' => 1,
          'masonry_resizable' => 1,
          'masonry_rtl' => 0,
          'masonry_stamp_selector' => '',
        ),
        'type' => 'text_default',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_external_content',
    'label' => 'Native Widget Code',
    'required' => 1,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
        'soft_length_limit' => '',
        'soft_length_minimum' => '',
        'soft_length_style_select' => 0,
      ),
      'type' => 'text_textarea',
      'weight' => 8,
    ),
  );

  // Exported field_instance:
  // 'node-add_news_article-field_featured_article_ad_code'.
  $field_instances['node-add_news_article-field_featured_article_ad_code'] = array(
    'bundle' => 'add_news_article',
    'default_value' => array(
      0 => array(
        'value' => '<div id=\'Incontent4\'></div>',
        'format' => 'php_code',
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'masonry' => FALSE,
          'masonry_animated' => 0,
          'masonry_animation_duration' => 500,
          'masonry_column_width' => '',
          'masonry_column_width_units' => 'px',
          'masonry_fit_width' => 0,
          'masonry_gutter_width' => 0,
          'masonry_images_first' => 1,
          'masonry_resizable' => 1,
          'masonry_rtl' => 0,
          'masonry_stamp_selector' => '',
        ),
        'type' => 'text_default',
        'weight' => 16,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_featured_article_ad_code',
    'label' => 'Banner Ad Code',
    'required' => 1,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
        'soft_length_limit' => '',
        'soft_length_minimum' => '',
        'soft_length_style_select' => 0,
      ),
      'type' => 'text_textarea',
      'weight' => 7,
    ),
  );

  // Exported field_instance:
  // 'node-add_news_article-field_featured_article_image'.
  $field_instances['node-add_news_article-field_featured_article_image'] = array(
    'bundle' => 'add_news_article',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
          'masonry' => FALSE,
          'masonry_animated' => 0,
          'masonry_animation_duration' => 500,
          'masonry_column_width' => '',
          'masonry_column_width_units' => 'px',
          'masonry_fit_width' => 0,
          'masonry_gutter_width' => 0,
          'masonry_images_first' => 1,
          'masonry_resizable' => 1,
          'masonry_rtl' => 0,
          'masonry_stamp_selector' => '',
        ),
        'type' => 'image',
        'weight' => 14,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_featured_article_image',
    'label' => 'Featured Article Image',
    'required' => 1,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'private' => 0,
          'public' => 'public',
        ),
        'allowed_types' => array(
          'audio' => 0,
          'document' => 0,
          'image' => 'image',
          'video' => 0,
        ),
        'browser_plugins' => array(
          'media_default--media_browser_1' => 'media_default--media_browser_1',
          'media_default--media_browser_my_files' => 'media_default--media_browser_my_files',
          'upload' => 'upload',
        ),
      ),
      'type' => 'media_generic',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'node-add_news_article-field_featured_article_logo'.
  $field_instances['node-add_news_article-field_featured_article_logo'] = array(
    'bundle' => 'add_news_article',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 15,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_featured_article_logo',
    'label' => 'Featured Article Logo',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'private' => 0,
          'public' => 'public',
        ),
        'allowed_types' => array(
          'audio' => 0,
          'document' => 0,
          'image' => 'image',
          'video' => 0,
        ),
        'browser_plugins' => array(
          'media_default--media_browser_1' => 'media_default--media_browser_1',
          'media_default--media_browser_my_files' => 'media_default--media_browser_my_files',
          'upload' => 'upload',
        ),
      ),
      'type' => 'media_generic',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-add_news_article-field_news_category'.
  $field_instances['node-add_news_article-field_news_category'] = array(
    'bundle' => 'add_news_article',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_news_category',
    'label' => 'Category',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-add_news_article-field_news_hashtags'.
  $field_instances['node-add_news_article-field_news_hashtags'] = array(
    'bundle' => 'add_news_article',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_news_hashtags',
    'label' => 'Hashtags',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'autocomplete_deluxe',
      'settings' => array(
        'autocomplete_deluxe_path' => 'autocomplete_deluxe/taxonomy',
        'autocomplete_path' => 'taxonomy/autocomplete',
        'delimiter' => '',
        'limit' => 10,
        'min_length' => 0,
        'not_found_message' => 'The term \'@term\' will be added.',
        'size' => 60,
      ),
      'type' => 'autocomplete_deluxe_taxonomy',
      'weight' => 5,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Banner Ad Code');
  t('Body');
  t('Category');
  t('Featured Article Image');
  t('Featured Article Logo');
  t('Hashtags');
  t('Native Widget Code');

  return $field_instances;
}
