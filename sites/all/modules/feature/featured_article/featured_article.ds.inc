<?php
/**
 * @file
 * featured_article.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function featured_article_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|add_news_article|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'add_news_article';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'post_date' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'ds_post_date_long',
    ),
  );
  $export['node|add_news_article|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function featured_article_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|add_news_article|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'add_news_article';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'bootstrap_6_6_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'top' => array(
        0 => 'field_news_category',
        1 => 'field_news_hashtags',
        2 => 'post_date',
      ),
      'left' => array(
        3 => 'body',
      ),
      'bottom' => array(
        4 => 'field_external_content',
        5 => 'disqus',
      ),
      'right' => array(
        6 => 'field_featured_article_image',
        7 => 'field_featured_article_ad_code',
      ),
    ),
    'fields' => array(
      'field_news_category' => 'top',
      'field_news_hashtags' => 'top',
      'post_date' => 'top',
      'body' => 'left',
      'field_external_content' => 'bottom',
      'disqus' => 'bottom',
      'field_featured_article_image' => 'right',
      'field_featured_article_ad_code' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
      'left' => array(
        'col-sm-12' => 'col-sm-12',
        'col-xs-12' => 'col-xs-12',
        'pull-xs-right' => 'pull-xs-right',
        'pull-sm-right' => 'pull-sm-right',
      ),
      'right' => array(
        'col-sm-12' => 'col-sm-12',
        'col-xs-12' => 'col-xs-12',
        'pull-xs-left' => 'pull-xs-left',
        'pull-sm-left' => 'pull-sm-left',
      ),
    ),
    'wrappers' => array(
      'top' => 'div',
      'left' => 'div',
      'right' => 'div',
      'bottom' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|add_news_article|default'] = $ds_layout;

  return $export;
}
