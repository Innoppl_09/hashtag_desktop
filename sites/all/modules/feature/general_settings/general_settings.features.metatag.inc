<?php
/**
 * @file
 * general_settings.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function general_settings_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: global.
  $config['global'] = array(
    'instance' => 'global',
    'disabled' => FALSE,
    'config' => array(
      'title' => array(
        'value' => '[current-page:title] | [current-page:pager][site:name]',
      ),
      'description' => array(
        'value' => 'We are simple minded but broadly focused. Our beliefs are closely tied to who we are and we use them like a beacon to guide us, and hopefully you, in this sea of partisan news.  ',
      ),
      'abstract' => array(
        'value' => 'We are simple minded but broadly focused. Our beliefs are closely tied to who we are and we use them like a beacon to guide us, and hopefully you, in this sea of partisan news.  We have developed cool technology that allows our readers to see Real Trending News, from Real News sources, in Real-Time. We follow ALL social media accounts, among all the social media networks. We track what is the most relevant news that may impact your world the most! It will not always be mainstream, but it will be what people are re-tweeting, liking, or sharing. Even though we may not have created the news you are consuming, we make sure it adheres to the standards we hold for ourselve may impact your world the most.',
      ),
      'keywords' => array(
        'value' => '#News, #HashTag, #Hash, #Tag, #HashTagNews, News, HashTag, Hash, Tag, HashTagNews, Hash Tag News',
      ),
      'robots' => array(
        'value' => array(
          'index' => 0,
          'follow' => 0,
          'noindex' => 0,
          'nofollow' => 0,
          'noarchive' => 0,
          'nosnippet' => 0,
          'noodp' => 0,
          'noydir' => 0,
          'noimageindex' => 0,
          'notranslate' => 0,
        ),
      ),
      'news_keywords' => array(
        'value' => '',
      ),
      'standout' => array(
        'value' => '',
      ),
      'rating' => array(
        'value' => '',
      ),
      'referrer' => array(
        'value' => '',
      ),
      'generator' => array(
        'value' => 'Drupal 7 (http://drupal.org)',
      ),
      'rights' => array(
        'value' => '',
      ),
      'image_src' => array(
        'value' => '',
      ),
      'canonical' => array(
        'value' => '[current-page:url:absolute]',
      ),
      'shortlink' => array(
        'value' => '[current-page:url:unaliased]',
      ),
      'original-source' => array(
        'value' => '',
      ),
      'prev' => array(
        'value' => '',
      ),
      'next' => array(
        'value' => '',
      ),
      'content-language' => array(
        'value' => '',
      ),
      'geo.position' => array(
        'value' => '',
      ),
      'geo.placename' => array(
        'value' => '',
      ),
      'geo.region' => array(
        'value' => '',
      ),
      'icbm' => array(
        'value' => '',
      ),
      'refresh' => array(
        'value' => '',
      ),
      'revisit-after' => array(
        'value' => '',
        'period' => '',
      ),
      'pragma' => array(
        'value' => '',
      ),
      'cache-control' => array(
        'value' => '',
      ),
      'expires' => array(
        'value' => '',
      ),
      'dcterms.title' => array(
        'value' => '[current-page:title]',
      ),
      'dcterms.creator' => array(
        'value' => '',
      ),
      'dcterms.subject' => array(
        'value' => '',
      ),
      'dcterms.description' => array(
        'value' => '',
      ),
      'dcterms.publisher' => array(
        'value' => '',
      ),
      'dcterms.contributor' => array(
        'value' => '',
      ),
      'dcterms.date' => array(
        'value' => '',
      ),
      'dcterms.type' => array(
        'value' => 'Text',
      ),
      'dcterms.format' => array(
        'value' => 'text/html',
      ),
      'dcterms.identifier' => array(
        'value' => '[current-page:url:absolute]',
      ),
      'dcterms.source' => array(
        'value' => '',
      ),
      'dcterms.language' => array(
        'value' => '',
      ),
      'dcterms.relation' => array(
        'value' => '',
      ),
      'dcterms.coverage' => array(
        'value' => '',
      ),
      'dcterms.rights' => array(
        'value' => '',
      ),
      'dcterms.abstract' => array(
        'value' => '',
      ),
      'dcterms.accessRights' => array(
        'value' => '',
      ),
      'dcterms.accrualMethod' => array(
        'value' => '',
      ),
      'dcterms.accrualPeriodicity' => array(
        'value' => '',
      ),
      'dcterms.accrualPolicy' => array(
        'value' => '',
      ),
      'dcterms.alternative' => array(
        'value' => '',
      ),
      'dcterms.audience' => array(
        'value' => '',
      ),
      'dcterms.available' => array(
        'value' => '',
      ),
      'dcterms.bibliographicCitation' => array(
        'value' => '',
      ),
      'dcterms.conformsTo' => array(
        'value' => '',
      ),
      'dcterms.created' => array(
        'value' => '',
      ),
      'dcterms.dateAccepted' => array(
        'value' => '',
      ),
      'dcterms.dateCopyrighted' => array(
        'value' => '',
      ),
      'dcterms.dateSubmitted' => array(
        'value' => '',
      ),
      'dcterms.educationLevel' => array(
        'value' => '',
      ),
      'dcterms.extent' => array(
        'value' => '',
      ),
      'dcterms.hasFormat' => array(
        'value' => '',
      ),
      'dcterms.hasPart' => array(
        'value' => '',
      ),
      'dcterms.hasVersion' => array(
        'value' => '',
      ),
      'dcterms.instructionalMethod' => array(
        'value' => '',
      ),
      'dcterms.isFormatOf' => array(
        'value' => '',
      ),
      'dcterms.isPartOf' => array(
        'value' => '',
      ),
      'dcterms.isReferencedBy' => array(
        'value' => '',
      ),
      'dcterms.isReplacedBy' => array(
        'value' => '',
      ),
      'dcterms.isRequiredBy' => array(
        'value' => '',
      ),
      'dcterms.isVersionOf' => array(
        'value' => '',
      ),
      'dcterms.issued' => array(
        'value' => '',
      ),
      'dcterms.license' => array(
        'value' => '',
      ),
      'dcterms.mediator' => array(
        'value' => '',
      ),
      'dcterms.medium' => array(
        'value' => '',
      ),
      'dcterms.modified' => array(
        'value' => '',
      ),
      'dcterms.provenance' => array(
        'value' => '',
      ),
      'dcterms.references' => array(
        'value' => '',
      ),
      'dcterms.replaces' => array(
        'value' => '',
      ),
      'dcterms.requires' => array(
        'value' => '',
      ),
      'dcterms.rightsHolder' => array(
        'value' => '',
      ),
      'dcterms.spatial' => array(
        'value' => '',
      ),
      'dcterms.tableOfContents' => array(
        'value' => '',
      ),
      'dcterms.temporal' => array(
        'value' => '',
      ),
      'dcterms.valid' => array(
        'value' => '',
      ),
      'fb:admins' => array(
        'value' => '',
      ),
      'fb:app_id' => array(
        'value' => '132626890718506',
      ),
      'fb:pages' => array(
        'value' => '',
      ),
      'shortcut icon' => array(
        'value' => 'https://www.thehashtag.news/sites/all/themes/hashtag/favicon.ico',
      ),
      'mask-icon' => array(
        'value' => '',
        'color' => '',
      ),
      'icon_16x16' => array(
        'value' => '',
      ),
      'icon_32x32' => array(
        'value' => '',
      ),
      'icon_96x96' => array(
        'value' => '',
      ),
      'icon_192x192' => array(
        'value' => '',
      ),
      'apple-touch-icon' => array(
        'value' => '',
      ),
      'apple-touch-icon_72x72' => array(
        'value' => '',
      ),
      'apple-touch-icon_76x76' => array(
        'value' => '',
      ),
      'apple-touch-icon_114x114' => array(
        'value' => '',
      ),
      'apple-touch-icon_120x120' => array(
        'value' => '',
      ),
      'apple-touch-icon_144x144' => array(
        'value' => '',
      ),
      'apple-touch-icon_152x152' => array(
        'value' => '',
      ),
      'apple-touch-icon_180x180' => array(
        'value' => '',
      ),
      'apple-touch-icon-precomposed' => array(
        'value' => '',
      ),
      'apple-touch-icon-precomposed_72x72' => array(
        'value' => '',
      ),
      'apple-touch-icon-precomposed_76x76' => array(
        'value' => '',
      ),
      'apple-touch-icon-precomposed_114x114' => array(
        'value' => '',
      ),
      'apple-touch-icon-precomposed_120x120' => array(
        'value' => '',
      ),
      'apple-touch-icon-precomposed_144x144' => array(
        'value' => '',
      ),
      'apple-touch-icon-precomposed_152x152' => array(
        'value' => '',
      ),
      'apple-touch-icon-precomposed_180x180' => array(
        'value' => '',
      ),
      'thumbnail' => array(
        'value' => '',
      ),
      'department' => array(
        'value' => '',
      ),
      'audience' => array(
        'value' => '',
      ),
      'doc_status' => array(
        'value' => '',
      ),
      'google_rating' => array(
        'value' => '',
      ),
      'itemtype' => array(
        'value' => '',
      ),
      'itemprop:name' => array(
        'value' => '[current-page:title]',
      ),
      'itemprop:description' => array(
        'value' => '',
      ),
      'itemprop:image' => array(
        'value' => '',
      ),
      'author' => array(
        'value' => '',
      ),
      'publisher' => array(
        'value' => '',
      ),
      'theme-color' => array(
        'value' => '',
      ),
      'MobileOptimized' => array(
        'value' => '',
      ),
      'HandheldFriendly' => array(
        'value' => '',
      ),
      'viewport' => array(
        'value' => '',
      ),
      'cleartype' => array(
        'value' => '',
      ),
      'amphtml' => array(
        'value' => '',
      ),
      'alternate_handheld' => array(
        'value' => '',
      ),
      'apple-itunes-app' => array(
        'value' => '',
      ),
      'apple-mobile-web-app-capable' => array(
        'value' => '',
      ),
      'apple-mobile-web-app-status-bar-style' => array(
        'value' => '',
      ),
      'apple-mobile-web-app-title' => array(
        'value' => '',
      ),
      'format-detection' => array(
        'value' => '',
      ),
      'ios-app-link-alternative' => array(
        'value' => '',
      ),
      'android-app-link-alternative' => array(
        'value' => '',
      ),
      'android-manifest' => array(
        'value' => '',
      ),
      'x-ua-compatible' => array(
        'value' => '',
      ),
      'application-name' => array(
        'value' => '',
      ),
      'msapplication-allowDomainApiCalls' => array(
        'value' => '',
      ),
      'msapplication-allowDomainMetaTags' => array(
        'value' => '',
      ),
      'msapplication-badge' => array(
        'value' => '',
      ),
      'msapplication-config' => array(
        'value' => '',
      ),
      'msapplication-navbutton-color' => array(
        'value' => '',
      ),
      'msapplication-notification' => array(
        'value' => '',
      ),
      'msapplication-square150x150logo' => array(
        'value' => '',
      ),
      'msapplication-square310x310logo' => array(
        'value' => '',
      ),
      'msapplication-square70x70logo' => array(
        'value' => '',
      ),
      'msapplication-wide310x150logo' => array(
        'value' => '',
      ),
      'msapplication-starturl' => array(
        'value' => '',
      ),
      'msapplication-task' => array(
        'value' => '',
      ),
      'msapplication-task-separator' => array(
        'value' => '',
      ),
      'msapplication-tilecolor' => array(
        'value' => '',
      ),
      'msapplication-tileimage' => array(
        'value' => '',
      ),
      'msapplication-tooltip' => array(
        'value' => '',
      ),
      'msapplication-window' => array(
        'value' => '',
      ),
      'og:site_name' => array(
        'value' => '[site:name]',
      ),
      'og:type' => array(
        'value' => 'website',
      ),
      'og:url' => array(
        'value' => '[current-page:url:absolute]',
      ),
      'og:title' => array(
        'value' => '[current-page:title]',
      ),
      'og:determiner' => array(
        'value' => '',
      ),
      'og:description' => array(
        'value' => 'We are simple minded but broadly focused. Our beliefs are closely tied to who we are and we use them like a beacon to guide us, and hopefully you, in this sea of partisan news.  We have developed cool technology that allows our readers to see Real Trending News, from Real News sources, in Real-Time. We follow ALL social media accounts, among all the social media networks. We track what is the most relevant news that may impact your world the most! It will not always be mainstream, but it will be what people are re-tweeting, liking, or sharing. Even though we may not have created the news you are consuming, we make sure it adheres to the standards we hold for ourselve may impact your world the most',
      ),
      'og:updated_time' => array(
        'value' => '',
      ),
      'og:see_also' => array(
        'value' => '',
      ),
      'og:image' => array(
        'value' => '',
      ),
      'og:image:url' => array(
        'value' => '',
      ),
      'og:image:secure_url' => array(
        'value' => '',
      ),
      'og:image:type' => array(
        'value' => '',
      ),
      'og:image:width' => array(
        'value' => '',
      ),
      'og:image:height' => array(
        'value' => '',
      ),
      'og:latitude' => array(
        'value' => '',
      ),
      'og:longitude' => array(
        'value' => '',
      ),
      'og:street_address' => array(
        'value' => '',
      ),
      'og:locality' => array(
        'value' => '',
      ),
      'og:region' => array(
        'value' => '',
      ),
      'og:postal_code' => array(
        'value' => '',
      ),
      'og:country_name' => array(
        'value' => '',
      ),
      'og:email' => array(
        'value' => 'privacy@thehashtagnews.com',
      ),
      'og:phone_number' => array(
        'value' => '',
      ),
      'og:fax_number' => array(
        'value' => '',
      ),
      'og:locale' => array(
        'value' => '',
      ),
      'og:locale:alternate' => array(
        'value' => '',
      ),
      'article:author' => array(
        'value' => '',
      ),
      'article:publisher' => array(
        'value' => '',
      ),
      'article:section' => array(
        'value' => '',
      ),
      'article:tag' => array(
        'value' => '',
      ),
      'article:published_time' => array(
        'value' => '',
      ),
      'article:modified_time' => array(
        'value' => '',
      ),
      'article:expiration_time' => array(
        'value' => '',
      ),
      'profile:first_name' => array(
        'value' => '',
      ),
      'profile:last_name' => array(
        'value' => '',
      ),
      'profile:username' => array(
        'value' => '',
      ),
      'profile:gender' => array(
        'value' => '',
      ),
      'og:audio' => array(
        'value' => '',
      ),
      'og:audio:secure_url' => array(
        'value' => '',
      ),
      'og:audio:type' => array(
        'value' => '',
      ),
      'book:author' => array(
        'value' => '',
      ),
      'book:isbn' => array(
        'value' => '',
      ),
      'book:release_date' => array(
        'value' => '',
      ),
      'book:tag' => array(
        'value' => '',
      ),
      'og:video:url' => array(
        'value' => '',
      ),
      'og:video:secure_url' => array(
        'value' => '',
      ),
      'og:video:width' => array(
        'value' => '',
      ),
      'og:video:height' => array(
        'value' => '',
      ),
      'og:video:type' => array(
        'value' => '',
      ),
      'video:actor' => array(
        'value' => '',
      ),
      'video:actor:role' => array(
        'value' => '',
      ),
      'video:director' => array(
        'value' => '',
      ),
      'video:writer' => array(
        'value' => '',
      ),
      'video:duration' => array(
        'value' => '',
      ),
      'video:release_date' => array(
        'value' => '',
      ),
      'video:tag' => array(
        'value' => '',
      ),
      'video:series' => array(
        'value' => '',
      ),
      'product:price:amount' => array(
        'value' => '',
      ),
      'product:price:currency' => array(
        'value' => '',
      ),
      'product:availability' => array(
        'value' => '',
      ),
      'product:brand' => array(
        'value' => '',
      ),
      'product:upc' => array(
        'value' => '',
      ),
      'product:ean' => array(
        'value' => '',
      ),
      'product:isbn' => array(
        'value' => '',
      ),
      'product:plural_title' => array(
        'value' => '',
      ),
      'product:retailer' => array(
        'value' => '',
      ),
      'product:retailer_title' => array(
        'value' => '',
      ),
      'product:retailer_part_no' => array(
        'value' => '',
      ),
      'product:mfr_part_no' => array(
        'value' => '',
      ),
      'product:size' => array(
        'value' => '',
      ),
      'product:product_link' => array(
        'value' => '',
      ),
      'product:category' => array(
        'value' => '',
      ),
      'product:color' => array(
        'value' => '',
      ),
      'product:material' => array(
        'value' => '',
      ),
      'product:pattern' => array(
        'value' => '',
      ),
      'product:shipping_cost:amount' => array(
        'value' => '',
      ),
      'product:shipping_cost:currency' => array(
        'value' => '',
      ),
      'product:weight:value' => array(
        'value' => '',
      ),
      'product:weight:units' => array(
        'value' => '',
      ),
      'product:shipping_weight:value' => array(
        'value' => '',
      ),
      'product:shipping_weight:units' => array(
        'value' => '',
      ),
      'product:expiration_time' => array(
        'value' => '',
      ),
      'product:condition' => array(
        'value' => '',
      ),
      'twitter:card' => array(
        'value' => 'summary',
      ),
      'twitter:site' => array(
        'value' => '',
      ),
      'twitter:site:id' => array(
        'value' => '',
      ),
      'twitter:creator' => array(
        'value' => '',
      ),
      'twitter:creator:id' => array(
        'value' => '',
      ),
      'twitter:url' => array(
        'value' => '[current-page:url:absolute]',
      ),
      'twitter:title' => array(
        'value' => '[current-page:title]',
      ),
      'twitter:description' => array(
        'value' => '',
      ),
      'twitter:image' => array(
        'value' => '',
      ),
      'twitter:image:width' => array(
        'value' => '',
      ),
      'twitter:image:height' => array(
        'value' => '',
      ),
      'twitter:image:alt' => array(
        'value' => '',
      ),
      'twitter:image0' => array(
        'value' => '',
      ),
      'twitter:image1' => array(
        'value' => '',
      ),
      'twitter:image2' => array(
        'value' => '',
      ),
      'twitter:image3' => array(
        'value' => '',
      ),
      'twitter:player' => array(
        'value' => '',
      ),
      'twitter:player:width' => array(
        'value' => '',
      ),
      'twitter:player:height' => array(
        'value' => '',
      ),
      'twitter:player:stream' => array(
        'value' => '',
      ),
      'twitter:player:stream:content_type' => array(
        'value' => '',
      ),
      'twitter:app:country' => array(
        'value' => '',
      ),
      'twitter:app:name:iphone' => array(
        'value' => '',
      ),
      'twitter:app:id:iphone' => array(
        'value' => '',
      ),
      'twitter:app:url:iphone' => array(
        'value' => '',
      ),
      'twitter:app:name:ipad' => array(
        'value' => '',
      ),
      'twitter:app:id:ipad' => array(
        'value' => '',
      ),
      'twitter:app:url:ipad' => array(
        'value' => '',
      ),
      'twitter:app:name:googleplay' => array(
        'value' => '',
      ),
      'twitter:app:id:googleplay' => array(
        'value' => '',
      ),
      'twitter:app:url:googleplay' => array(
        'value' => '',
      ),
      'twitter:label1' => array(
        'value' => '',
      ),
      'twitter:data1' => array(
        'value' => '',
      ),
      'twitter:label2' => array(
        'value' => '',
      ),
      'twitter:data2' => array(
        'value' => '',
      ),
      'msvalidate.01' => array(
        'value' => '',
      ),
      'baidu-site-verification' => array(
        'value' => '',
      ),
      'google-site-verification' => array(
        'value' => '',
      ),
      'norton-safeweb-site-verification' => array(
        'value' => '',
      ),
      'p:domain_verify' => array(
        'value' => '',
      ),
      'yandex-verification' => array(
        'value' => '',
      ),
    ),
  );

  // Exported Metatag config instance: global:403.
  $config['global:403'] = array(
    'instance' => 'global:403',
    'disabled' => FALSE,
    'config' => array(
      'canonical' => array(
        'value' => '[site:url]',
      ),
      'shortlink' => array(
        'value' => '[site:url]',
      ),
      'dcterms.identifier' => array(
        'value' => '[site:url]',
      ),
      'dcterms.title' => array(
        'value' => '[site:name]',
      ),
      'itemprop:name' => array(
        'value' => '[site:name]',
      ),
      'og:title' => array(
        'value' => '[site:name]',
      ),
      'og:type' => array(
        'value' => 'website',
      ),
      'og:url' => array(
        'value' => '[site:url]',
      ),
      'twitter:title' => array(
        'value' => '[site:name]',
      ),
      'twitter:url' => array(
        'value' => '[site:url]',
      ),
    ),
  );

  // Exported Metatag config instance: global:404.
  $config['global:404'] = array(
    'instance' => 'global:404',
    'disabled' => FALSE,
    'config' => array(
      'canonical' => array(
        'value' => '[site:url]',
      ),
      'shortlink' => array(
        'value' => '[site:url]',
      ),
      'dcterms.identifier' => array(
        'value' => '[site:url]',
      ),
      'dcterms.title' => array(
        'value' => '[site:name]',
      ),
      'itemprop:name' => array(
        'value' => '[site:name]',
      ),
      'og:title' => array(
        'value' => '[site:name]',
      ),
      'og:type' => array(
        'value' => 'website',
      ),
      'og:url' => array(
        'value' => '[site:url]',
      ),
      'twitter:title' => array(
        'value' => '[site:name]',
      ),
      'twitter:url' => array(
        'value' => '[site:url]',
      ),
    ),
  );

  // Exported Metatag config instance: global:frontpage.
  $config['global:frontpage'] = array(
    'instance' => 'global:frontpage',
    'disabled' => FALSE,
    'config' => array(
      'title' => array(
        'value' => '[site:name] | [current-page:pager]',
      ),
      'image_src' => array(
        'value' => '[site:url]sites/all/themes/hashtag/logo_200.png',
      ),
      'canonical' => array(
        'value' => '[site:url]',
      ),
      'shortlink' => array(
        'value' => '[site:url]',
      ),
      'dcterms.title' => array(
        'value' => '[site:name]',
      ),
      'dcterms.description' => array(
        'value' => '[site:slogan]',
      ),
      'dcterms.identifier' => array(
        'value' => '[site:url]',
      ),
      'itemprop:name' => array(
        'value' => '[site:name]',
      ),
      'itemprop:description' => array(
        'value' => '[site:slogan]',
      ),
      'og:url' => array(
        'value' => '[site:url]',
      ),
      'og:title' => array(
        'value' => '[site:name]',
      ),
      'og:description' => array(
        'value' => '[site:slogan]',
      ),
      'twitter:url' => array(
        'value' => '[site:url]',
      ),
      'twitter:title' => array(
        'value' => '[site:name]',
      ),
      'twitter:description' => array(
        'value' => '[site:slogan]',
      ),
    ),
  );

  // Exported Metatag config instance: node.
  $config['node'] = array(
    'instance' => 'node',
    'disabled' => FALSE,
    'config' => array(
      'title' => array(
        'value' => '[node:title] | [current-page:pager][site:name]',
      ),
      'description' => array(
        'value' => '[node:summary]',
      ),
      'dcterms.creator' => array(
        'value' => '[node:author]',
      ),
      'dcterms.date' => array(
        'value' => '[node:created:custom:Y-m-d\\TH:iP]',
      ),
      'dcterms.description' => array(
        'value' => '[node:summary]',
      ),
      'dcterms.language' => array(
        'value' => '[node:language]',
      ),
      'dcterms.title' => array(
        'value' => '[node:title]',
      ),
      'dcterms.modified' => array(
        'value' => '[node:changed:custom:Y-m-d\\TH:iP]',
      ),
      'itemprop:description' => array(
        'value' => '[node:summary]',
      ),
      'itemprop:name' => array(
        'value' => '[node:title]',
      ),
      'itemtype' => array(
        'value' => 'Article',
      ),
      'article:modified_time' => array(
        'value' => '[node:changed:custom:c]',
      ),
      'article:published_time' => array(
        'value' => '[node:created:custom:c]',
      ),
      'og:description' => array(
        'value' => '[node:summary]',
      ),
      'og:title' => array(
        'value' => '[node:title]',
      ),
      'og:updated_time' => array(
        'value' => '[node:changed:custom:c]',
      ),
      'twitter:description' => array(
        'value' => '[node:summary]',
      ),
      'twitter:title' => array(
        'value' => '[node:title]',
      ),
    ),
  );

  // Exported Metatag config instance: node:add_news_article.
  $config['node:add_news_article'] = array(
    'instance' => 'node:add_news_article',
    'disabled' => FALSE,
    'config' => array(
      'abstract' => array(
        'value' => '[node:summary]',
      ),
      'robots' => array(
        'value' => array(
          'index' => 'index',
          'follow' => 'follow',
          'noindex' => 0,
          'nofollow' => 0,
          'noarchive' => 0,
          'nosnippet' => 0,
          'noodp' => 0,
          'noydir' => 0,
          'noimageindex' => 0,
          'notranslate' => 0,
        ),
      ),
      'rating' => array(
        'value' => 'mature',
      ),
      'image_src' => array(
        'value' => '[node:field_featured_article_image]',
      ),
      'og:image' => array(
        'value' => '[node:field_featured_article_image]',
      ),
      'og:image:url' => array(
        'value' => '[node:field_featured_article_image]',
      ),
      'og:image:width' => array(
        'value' => '',
      ),
      'og:image:height' => array(
        'value' => '',
      ),
      'og:email' => array(
        'value' => 'privacy@thehashtagnews.com',
      ),
    ),
  );

  // Exported Metatag config instance: taxonomy_term.
  $config['taxonomy_term'] = array(
    'instance' => 'taxonomy_term',
    'disabled' => FALSE,
    'config' => array(
      'title' => array(
        'value' => '[term:name] | [current-page:pager][site:name]',
      ),
      'description' => array(
        'value' => '[term:description]',
      ),
      'dcterms.description' => array(
        'value' => '[term:description]',
      ),
      'dcterms.title' => array(
        'value' => '[term:name]',
      ),
      'itemprop:description' => array(
        'value' => '[term:description]',
      ),
      'itemprop:name' => array(
        'value' => '[term:name]',
      ),
      'og:description' => array(
        'value' => '[term:description]',
      ),
      'og:title' => array(
        'value' => '[term:name]',
      ),
      'twitter:description' => array(
        'value' => '[term:description]',
      ),
      'twitter:title' => array(
        'value' => '[term:name]',
      ),
    ),
  );

  // Exported Metatag config instance: user.
  $config['user'] = array(
    'instance' => 'user',
    'disabled' => FALSE,
    'config' => array(
      'title' => array(
        'value' => '[user:name] | [site:name]',
      ),
      'image_src' => array(
        'value' => '[user:picture:url]',
      ),
      'dcterms.creator' => array(
        'value' => '[user:name]',
      ),
      'dcterms.date' => array(
        'value' => '[user:created:custom:Y-m-d\\TH:iP]',
      ),
      'dcterms.title' => array(
        'value' => '[user:name]',
      ),
      'itemprop:name' => array(
        'value' => '[user:name]',
      ),
      'itemtype' => array(
        'value' => 'Person',
      ),
      'itemprop:image' => array(
        'value' => '[user:picture:url]',
      ),
      'og:title' => array(
        'value' => '[user:name]',
      ),
      'og:type' => array(
        'value' => 'profile',
      ),
      'profile:username' => array(
        'value' => '[user:name]',
      ),
      'og:image' => array(
        'value' => '[user:picture:url]',
      ),
      'twitter:title' => array(
        'value' => '[user:name]',
      ),
      'twitter:image' => array(
        'value' => '[user:picture:url]',
      ),
    ),
  );

  // Exported Metatag config instance: view.
  $config['view'] = array(
    'instance' => 'view',
    'disabled' => FALSE,
    'config' => array(
      'title' => array(
        'value' => '[view:title] | [current-page:pager][site:name]',
      ),
      'description' => array(
        'value' => '[view:description]',
      ),
      'canonical' => array(
        'value' => '[view:url]',
      ),
    ),
  );

  return $config;
}
