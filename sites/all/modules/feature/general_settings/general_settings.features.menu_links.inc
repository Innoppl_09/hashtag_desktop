<?php
/**
 * @file
 * general_settings.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function general_settings_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_business:<void>.
  $menu_links['main-menu_business:<void>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<void>',
    'router_path' => '<void>',
    'link_title' => 'Business',
    'options' => array(
      'attributes' => array(),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'main-menu_business:<void>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_entertainment:<void>.
  $menu_links['main-menu_entertainment:<void>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<void>',
    'router_path' => '<void>',
    'link_title' => '#Entertainment',
    'options' => array(
      'attributes' => array(),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'main-menu_entertainment:<void>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: main-menu_home:<front>.
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => '#Home',
    'options' => array(
      'external' => 0,
      'attributes' => array(),
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_more-1:<void>.
  $menu_links['main-menu_more-1:<void>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<void>',
    'router_path' => '<void>',
    'link_title' => '#More 1',
    'options' => array(
      'attributes' => array(),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'main-menu_more-1:<void>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_more:<void>',
  );
  // Exported menu link: main-menu_more-2:<void>.
  $menu_links['main-menu_more-2:<void>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<void>',
    'router_path' => '<void>',
    'link_title' => '#More 2',
    'options' => array(
      'attributes' => array(),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'main-menu_more-2:<void>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_more:<void>',
  );
  // Exported menu link: main-menu_more:<void>.
  $menu_links['main-menu_more:<void>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<void>',
    'router_path' => '<void>',
    'link_title' => '#More',
    'options' => array(
      'attributes' => array(),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'main-menu_more:<void>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: main-menu_politics:<void>.
  $menu_links['main-menu_politics:<void>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<void>',
    'router_path' => '<void>',
    'link_title' => '#Politics',
    'options' => array(
      'attributes' => array(),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'main-menu_politics:<void>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_sports:<void>.
  $menu_links['main-menu_sports:<void>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<void>',
    'router_path' => '<void>',
    'link_title' => '#Sports',
    'options' => array(
      'attributes' => array(),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'main-menu_sports:<void>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_technology:<void>.
  $menu_links['main-menu_technology:<void>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<void>',
    'router_path' => '<void>',
    'link_title' => '#Technology',
    'options' => array(
      'attributes' => array(),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'main-menu_technology:<void>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-mobile-menu_breaking:breaking.
  $menu_links['menu-mobile-menu_breaking:breaking'] = array(
    'menu_name' => 'menu-mobile-menu',
    'link_path' => 'breaking',
    'router_path' => 'breaking',
    'link_title' => '#Breaking',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-mobile-menu_breaking:breaking',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-mobile-menu_business:business.
  $menu_links['menu-mobile-menu_business:business'] = array(
    'menu_name' => 'menu-mobile-menu',
    'link_path' => 'business',
    'router_path' => 'business',
    'link_title' => '#Business',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-mobile-menu_business:business',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-mobile-menu_editorial:editorial.
  $menu_links['menu-mobile-menu_editorial:editorial'] = array(
    'menu_name' => 'menu-mobile-menu',
    'link_path' => 'editorial',
    'router_path' => 'editorial',
    'link_title' => '#Editorial',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-mobile-menu_editorial:editorial',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-mobile-menu_entertainment:entertainment.
  $menu_links['menu-mobile-menu_entertainment:entertainment'] = array(
    'menu_name' => 'menu-mobile-menu',
    'link_path' => 'entertainment',
    'router_path' => 'entertainment',
    'link_title' => '#Entertainment',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-mobile-menu_entertainment:entertainment',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-mobile-menu_home:<front>.
  $menu_links['menu-mobile-menu_home:<front>'] = array(
    'menu_name' => 'menu-mobile-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'external' => 0,
      'identifier' => 'menu-mobile-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-mobile-menu_politics:politics.
  $menu_links['menu-mobile-menu_politics:politics'] = array(
    'menu_name' => 'menu-mobile-menu',
    'link_path' => 'politics',
    'router_path' => 'politics',
    'link_title' => '#Politics',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-mobile-menu_politics:politics',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: menu-mobile-menu_sports:sports.
  $menu_links['menu-mobile-menu_sports:sports'] = array(
    'menu_name' => 'menu-mobile-menu',
    'link_path' => 'sports',
    'router_path' => 'sports',
    'link_title' => '#Sports',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-mobile-menu_sports:sports',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: menu-news-category_breaking:breaking.
  $menu_links['menu-news-category_breaking:breaking'] = array(
    'menu_name' => 'menu-news-category',
    'link_path' => 'breaking',
    'router_path' => 'breaking',
    'link_title' => '#Breaking',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'col-md-2 col-sm-2 col-xs-4 menu-image-breaking menu-image',
        'style' => '',
      ),
      'external' => 0,
      'identifier' => 'menu-news-category_breaking:breaking',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-news-category_business:business.
  $menu_links['menu-news-category_business:business'] = array(
    'menu_name' => 'menu-news-category',
    'link_path' => 'business',
    'router_path' => 'business',
    'link_title' => '#Business',
    'options' => array(
      'external' => 0,
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'col-md-2 col-sm-2 col-xs-4 menu-image-business menu-image',
        'style' => '',
      ),
      'identifier' => 'menu-news-category_business:business',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-news-category_editorial:editorial.
  $menu_links['menu-news-category_editorial:editorial'] = array(
    'menu_name' => 'menu-news-category',
    'link_path' => 'editorial',
    'router_path' => 'editorial',
    'link_title' => '#Editorial',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'col-md-2 col-sm-2 col-xs-4 menu-image-editorial menu-image',
        'style' => '',
      ),
      'external' => 0,
      'identifier' => 'menu-news-category_editorial:editorial',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-news-category_entertainment:entertainment.
  $menu_links['menu-news-category_entertainment:entertainment'] = array(
    'menu_name' => 'menu-news-category',
    'link_path' => 'entertainment',
    'router_path' => 'entertainment',
    'link_title' => '#Entertainment',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'col-md-2 col-sm-2 col-xs-4 menu-image-entertainment menu-image',
        'style' => '',
      ),
      'external' => 0,
      'identifier' => 'menu-news-category_entertainment:entertainment',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: menu-news-category_politics:politics.
  $menu_links['menu-news-category_politics:politics'] = array(
    'menu_name' => 'menu-news-category',
    'link_path' => 'politics',
    'router_path' => 'politics',
    'link_title' => '#Politics',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'col-md-2 col-sm-2 col-xs-4 menu-image-politics menu-image',
        'style' => '',
      ),
      'external' => 0,
      'identifier' => 'menu-news-category_politics:politics',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-news-category_sports:sports.
  $menu_links['menu-news-category_sports:sports'] = array(
    'menu_name' => 'menu-news-category',
    'link_path' => 'sports',
    'router_path' => 'sports',
    'link_title' => '#Sports',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'col-md-2 col-sm-2 col-xs-4 menu-image-sports menu-image',
        'style' => '',
      ),
      'external' => 0,
      'identifier' => 'menu-news-category_sports:sports',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('#Breaking');
  t('#Business');
  t('#Editorial');
  t('#Entertainment');
  t('#Home');
  t('#More');
  t('#More 1');
  t('#More 2');
  t('#Politics');
  t('#Sports');
  t('#Technology');
  t('Business');
  t('Home');

  return $menu_links;
}
