<?php
/**
 * @file
 * general_settings.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function general_settings_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer remove_duplicates'.
  $permissions['administer remove_duplicates'] = array(
    'name' => 'administer remove_duplicates',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'remove_duplicates',
  );

  return $permissions;
}
