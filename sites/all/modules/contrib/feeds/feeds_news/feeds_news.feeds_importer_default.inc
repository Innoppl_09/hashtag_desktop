<?php
/**
 * @file
 * feeds_news.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function feeds_news_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'feed';
  $feeds_importer->config = array(
    'name' => 'Hashtag News Data Fetcher',
    'description' => 'Hashtag News Data Fetcher',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 1,
        'cache_http_result' => 1,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExJmesPath',
      'config' => array(
        'context' => array(
          'value' => 'response.data[*]',
        ),
        'sources' => array(
          'post_id' => array(
            'name' => 'post_id',
            'value' => 'post_id',
            'debug' => 0,
            'weight' => '1',
          ),
          'caption' => array(
            'name' => 'caption',
            'value' => 'caption',
            'debug' => 0,
            'weight' => '2',
          ),
          'created_at' => array(
            'name' => 'created_at',
            'value' => 'created_at',
            'debug' => 0,
            'weight' => '3',
          ),
          'rts' => array(
            'name' => 'rts',
            'value' => 'rts',
            'debug' => 0,
            'weight' => '6',
          ),
          'hashtags' => array(
            'name' => 'hashtags',
            'value' => 'hashtags',
            'debug' => 0,
            'weight' => '8',
          ),
          'link' => array(
            'name' => 'link',
            'value' => 'link',
            'debug' => 0,
            'weight' => '9',
          ),
          'media_list_type_link_url' => array(
            'name' => 'media_list[?type==\'link\'].url',
            'value' => 'media_list[?type==\'link\'].url',
            'debug' => 0,
            'weight' => '15',
          ),
        ),
        'display_errors' => 0,
        'debug_mode' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'bundle' => 'hash_tag_news',
        'update_existing' => '0',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'source_node:field_hashtag_account_category',
            'target' => 'field_news_category',
            'term_search' => '1',
            'autocreate' => 0,
            'language' => 'und',
          ),
          1 => array(
            'source' => 'post_id',
            'target' => 'guid',
            'unique' => 1,
          ),
          2 => array(
            'source' => 'caption',
            'target' => 'field_news_caption',
            'unique' => FALSE,
            'language' => 'und',
          ),
          3 => array(
            'source' => 'caption',
            'target' => 'title',
            'unique' => 1,
            'language' => 'und',
          ),
          4 => array(
            'source' => 'hashtags',
            'target' => 'field_news_hashtags',
            'term_search' => '0',
            'autocreate' => 1,
            'language' => 'und',
          ),
          5 => array(
            'source' => 'rts',
            'target' => 'field_news_retweets',
            'unique' => FALSE,
            'language' => 'und',
          ),
          6 => array(
            'source' => 'source_node:field_hashtag_account_name',
            'target' => 'field_news_fetch_account',
            'unique' => FALSE,
            'language' => 'und',
          ),
          7 => array(
            'source' => 'Blank source 5',
            'target' => 'field_news_source',
            'unique' => FALSE,
            'language' => 'und',
          ),
          8 => array(
            'source' => 'link',
            'target' => 'field_news_link:url',
            'unique' => FALSE,
            'language' => 'und',
          ),
          9 => array(
            'source' => 'media_list_type_link_url',
            'target' => 'field_news_media_link',
            'unique' => FALSE,
            'language' => 'und',
          ),
          10 => array(
            'source' => 'created_at',
            'target' => 'field_news_fetch_date:start',
            'timezone' => '__SITE__',
            'language' => 'und',
          ),
        ),
        'input_format' => 'full_html',
        'author' => '309',
        'authorize' => 1,
        'insert_new' => '1',
        'update_non_existent' => 'skip',
        'skip_hash_check' => 0,
        'language' => 'und',
      ),
    ),
    'content_type' => 'feed',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 1,
  );
  $export['feed'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'opml';
  $feeds_importer->config = array(
    'name' => 'OPML import',
    'description' => 'Import subscriptions from OPML files. Use together with "Feed" configuration.',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsOPMLParser',
      'config' => array(),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'bundle' => 'feed',
        'update_existing' => 0,
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'xmlurl',
            'target' => 'feeds_source',
            'unique' => 1,
          ),
        ),
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
  );
  $export['opml'] = $feeds_importer;

  return $export;
}
