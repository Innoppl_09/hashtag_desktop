<?php
/**
 * @file
 * feeds_news.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function feeds_news_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|hash_tag_news|news_listing';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'hash_tag_news';
  $ds_fieldsetting->view_mode = 'news_listing';
  $ds_fieldsetting->settings = array(
    'node_link' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link text' => 'View Article',
        'link class' => '',
        'wrapper' => '',
        'class' => '',
      ),
    ),
    'addtoany' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|hash_tag_news|news_listing'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function feeds_news_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|hash_tag_news|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'hash_tag_news';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'hashtag_90_10_layout';
  $ds_layout->settings = array(
    'regions' => array(
      'top' => array(
        0 => 'field_news_category',
        1 => 'field_news_caption',
      ),
      'left' => array(
        2 => 'field_news_hashtags',
      ),
      'right' => array(
        3 => 'field_news_retweets',
        4 => 'field_news_favorites',
      ),
      'bottom' => array(
        5 => 'disqus',
      ),
    ),
    'fields' => array(
      'field_news_category' => 'top',
      'field_news_caption' => 'top',
      'field_news_hashtags' => 'left',
      'field_news_retweets' => 'right',
      'field_news_favorites' => 'right',
      'disqus' => 'bottom',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'top' => 'div',
      'left' => 'div',
      'right' => 'div',
      'bottom' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|hash_tag_news|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|hash_tag_news|news_listing';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'hash_tag_news';
  $ds_layout->view_mode = 'news_listing';
  $ds_layout->layout = 'hashtag_90_10_layout';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_news_body',
        1 => 'field_news_hashtags',
        2 => 'addtoany',
        3 => 'node_link',
        4 => 'field_news_comments',
      ),
      'right' => array(
        5 => 'field_news_retweets',
        6 => 'field_news_favorites',
      ),
    ),
    'fields' => array(
      'field_news_body' => 'left',
      'field_news_hashtags' => 'left',
      'addtoany' => 'left',
      'node_link' => 'left',
      'field_news_comments' => 'left',
      'field_news_retweets' => 'right',
      'field_news_favorites' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'top' => 'div',
      'left' => 'div',
      'right' => 'div',
      'bottom' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|hash_tag_news|news_listing'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|hash_tag_news|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'hash_tag_news';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col_wrapper';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'body',
      ),
    ),
    'fields' => array(
      'body' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|hash_tag_news|teaser'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function feeds_news_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'news_listing';
  $ds_view_mode->label = 'News Listing';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['news_listing'] = $ds_view_mode;

  return $export;
}
