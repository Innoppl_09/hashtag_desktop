<?php
/**
 * @file
 * feeds_news.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function feeds_news_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-feed-field_hashtag_account_category'.
  $field_instances['node-feed-field_hashtag_account_category'] = array(
    'bundle' => 'feed',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_hashtag_account_category',
    'label' => 'Hashtag Account Category',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'options_select',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-feed-field_hashtag_account_hash'.
  $field_instances['node-feed-field_hashtag_account_hash'] = array(
    'bundle' => 'feed',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(
          'masonry' => FALSE,
          'masonry_animated' => 0,
          'masonry_animation_duration' => 500,
          'masonry_column_width' => '',
          'masonry_column_width_units' => 'px',
          'masonry_fit_width' => 0,
          'masonry_gutter_width' => 0,
          'masonry_images_first' => 1,
          'masonry_resizable' => 1,
          'masonry_rtl' => 0,
          'masonry_stamp_selector' => '',
        ),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_hashtag_account_hash',
    'label' => 'Hashtag Account hash',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
        'soft_length_limit' => '',
        'soft_length_minimum' => '',
        'soft_length_style_select' => 0,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-feed-field_hashtag_account_name'.
  $field_instances['node-feed-field_hashtag_account_name'] = array(
    'bundle' => 'feed',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(
          'masonry' => FALSE,
          'masonry_animated' => 0,
          'masonry_animation_duration' => 500,
          'masonry_column_width' => '',
          'masonry_column_width_units' => 'px',
          'masonry_fit_width' => 0,
          'masonry_gutter_width' => 0,
          'masonry_images_first' => 1,
          'masonry_resizable' => 1,
          'masonry_rtl' => 0,
          'masonry_stamp_selector' => '',
        ),
        'type' => 'text_default',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_hashtag_account_name',
    'label' => 'Hashtag Account Name',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
        'soft_length_limit' => '',
        'soft_length_minimum' => '',
        'soft_length_style_select' => 0,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-hash_tag_news-field_news_caption'.
  $field_instances['node-hash_tag_news-field_news_caption'] = array(
    'bundle' => 'hash_tag_news',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '<i class="fa fa-info-circle"></i>
- Write a title for all to see, this will give fellow users an idea of what to expect from your link',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'masonry' => FALSE,
          'masonry_animated' => 0,
          'masonry_animation_duration' => 500,
          'masonry_column_width' => '',
          'masonry_column_width_units' => 'px',
          'masonry_fit_width' => 0,
          'masonry_gutter_width' => 0,
          'masonry_images_first' => 1,
          'masonry_resizable' => 1,
          'masonry_rtl' => 0,
          'masonry_stamp_selector' => '',
        ),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'news_listing' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_news_caption',
    'label' => 'News Caption',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
        'soft_length_limit' => 230,
        'soft_length_minimum' => '',
        'soft_length_style_select' => 0,
      ),
      'type' => 'text_textfield',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-hash_tag_news-field_news_category'.
  $field_instances['node-hash_tag_news-field_news_category'] = array(
    'bundle' => 'hash_tag_news',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '<i class="fa fa-info-circle"></i>
- Select the category here',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 1,
      ),
      'news_listing' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 14,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_news_category',
    'label' => 'News Category',
    'required' => 1,
    'settings' => array(
      'field_tooltips_enabled' => 1,
      'field_tooltips_tooltip' => array(
        'format' => 'full_html',
        'value' => 'sdfsdflskdjfdslkfsd flsd lfk sdlkf sdlnflskd f',
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-hash_tag_news-field_news_fetch_account'.
  $field_instances['node-hash_tag_news-field_news_fetch_account'] = array(
    'bundle' => 'hash_tag_news',
    'default_value' => array(
      0 => array(
        'value' => '@accountname',
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 23,
      ),
      'news_listing' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_news_fetch_account',
    'label' => 'News Fetch Account',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
        'soft_length_limit' => '',
        'soft_length_minimum' => '',
        'soft_length_style_select' => 0,
      ),
      'type' => 'text_textfield',
      'weight' => 13,
    ),
  );

  // Exported field_instance: 'node-hash_tag_news-field_news_fetch_date'.
  $field_instances['node-hash_tag_news-field_news_fetch_date'] = array(
    'bundle' => 'hash_tag_news',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_remaining_days' => FALSE,
        ),
        'type' => 'date_default',
        'weight' => 34,
      ),
      'news_listing' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_news_fetch_date',
    'label' => 'News Fetch Date',
    'required' => 0,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 1,
        'input_format' => 'Y-m-d H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'no_fieldset' => 0,
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_text',
      'weight' => 27,
    ),
  );

  // Exported field_instance: 'node-hash_tag_news-field_news_hashtags'.
  $field_instances['node-hash_tag_news-field_news_hashtags'] = array(
    'bundle' => 'hash_tag_news',
    'default_value' => array(
      0 => array(
        'tid' => 18,
        'vid' => 6,
        'name' => '#bbc',
        'description' => '',
        'format' => 'filtered_html',
        'weight' => 0,
        'uuid' => '0c7d454e-2d5b-4a36-bf0e-bf95155b5bbd',
        'vocabulary_machine_name' => 'hashtag',
        'rdf_mapping' => array(
          'description' => array(
            'predicates' => array(
              0 => 'skos:definition',
            ),
          ),
          'name' => array(
            'predicates' => array(
              0 => 'rdfs:label',
              1 => 'skos:prefLabel',
            ),
          ),
          'parent' => array(
            'predicates' => array(
              0 => 'skos:broader',
            ),
            'type' => 'rel',
          ),
          'rdftype' => array(
            0 => 'skos:Concept',
          ),
          'vid' => array(
            'predicates' => array(
              0 => 'skos:inScheme',
            ),
            'type' => 'rel',
          ),
        ),
      ),
      1 => array(
        'description' => '',
        'format' => 'filtered_html',
        'name' => '#bbcnews',
        'rdf_mapping' => array(
          'description' => array(
            'predicates' => array(
              0 => 'skos:definition',
            ),
          ),
          'name' => array(
            'predicates' => array(
              0 => 'rdfs:label',
              1 => 'skos:prefLabel',
            ),
          ),
          'parent' => array(
            'predicates' => array(
              0 => 'skos:broader',
            ),
            'type' => 'rel',
          ),
          'rdftype' => array(
            0 => 'skos:Concept',
          ),
          'vid' => array(
            'predicates' => array(
              0 => 'skos:inScheme',
            ),
            'type' => 'rel',
          ),
        ),
        'tid' => 2,
        'uuid' => '32a7b5f8-4e16-4a97-852f-2b1bd795d70b',
        'vid' => 6,
        'vocabulary_machine_name' => 'hashtag',
        'weight' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 3,
      ),
      'news_listing' => array(
        'label' => 'hidden',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_news_hashtags',
    'label' => 'News Hashtags',
    'required' => 0,
    'settings' => array(
      'field_tooltips_enabled' => FALSE,
      'field_tooltips_tooltip' => array(
        'format' => NULL,
        'value' => '',
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'autocomplete_deluxe',
      'settings' => array(
        'autocomplete_deluxe_path' => 'autocomplete_deluxe/taxonomy',
        'autocomplete_path' => 'taxonomy/autocomplete',
        'delimiter' => '',
        'limit' => 10,
        'min_length' => 0,
        'not_found_message' => 'The term \'@term\' will be added.',
        'size' => 60,
      ),
      'type' => 'autocomplete_deluxe_taxonomy',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-hash_tag_news-field_news_link'.
  $field_instances['node-hash_tag_news-field_news_link'] = array(
    'bundle' => 'hash_tag_news',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '<i class="fa fa-info-circle"></i>
- Enter the link for your article then click “Check Link”',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 20,
      ),
      'news_listing' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_news_link',
    'label' => 'News Link',
    'required' => 0,
    'settings' => array(
      'absolute_url' => 1,
      'attributes' => array(
        'class' => '',
        'configurable_class' => 0,
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'rel_remove' => 'default',
      'title' => 'optional',
      'title_label_use_field_label' => 0,
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-hash_tag_news-field_news_markup'.
  $field_instances['node-hash_tag_news-field_news_markup'] = array(
    'bundle' => 'hash_tag_news',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 18,
      ),
      'news_listing' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 17,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_news_markup',
    'label' => 'News Markup',
    'required' => 0,
    'settings' => array(
      'field_tooltips_enabled' => FALSE,
      'field_tooltips_tooltip' => array(
        'format' => NULL,
        'value' => '',
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'markup',
      'settings' => array(),
      'type' => 'markup',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'node-hash_tag_news-field_news_media_link'.
  $field_instances['node-hash_tag_news-field_news_media_link'] = array(
    'bundle' => 'hash_tag_news',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 24,
      ),
      'news_listing' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_news_media_link',
    'label' => 'News Media Link',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
        'soft_length_limit' => '',
        'soft_length_minimum' => '',
        'soft_length_style_select' => 0,
      ),
      'type' => 'text_textfield',
      'weight' => 17,
    ),
  );

  // Exported field_instance: 'node-hash_tag_news-field_news_retweets'.
  $field_instances['node-hash_tag_news-field_news_retweets'] = array(
    'bundle' => 'hash_tag_news',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => 1,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_decimal',
        'weight' => 4,
      ),
      'news_listing' => array(
        'label' => 'hidden',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => 1,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_decimal',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_news_retweets',
    'label' => 'News Retweets',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => 0,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 10,
    ),
  );

  // Exported field_instance: 'node-hash_tag_news-field_news_source'.
  $field_instances['node-hash_tag_news-field_news_source'] = array(
    'bundle' => 'hash_tag_news',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '<i class="fa fa-info-circle"></i>
- Where did the article come from? CNN? BBC?',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 19,
      ),
      'news_listing' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_news_source',
    'label' => 'News Source',
    'required' => 0,
    'settings' => array(
      'field_tooltips_enabled' => 0,
      'field_tooltips_tooltip' => array(
        'format' => 'filtered_html',
        'value' => '',
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'taxonomy',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'taxonomy_autocomplete',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'taxonomy_term-category-field_tag_icon'.
  $field_instances['taxonomy_term-category-field_tag_icon'] = array(
    'bundle' => 'category',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(
          'masonry' => FALSE,
          'masonry_animated' => 0,
          'masonry_animation_duration' => 500,
          'masonry_column_width' => '',
          'masonry_column_width_units' => 'px',
          'masonry_fit_width' => 0,
          'masonry_gutter_width' => 0,
          'masonry_images_first' => 1,
          'masonry_resizable' => 1,
          'masonry_rtl' => 0,
          'masonry_stamp_selector' => '',
        ),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_tag_icon',
    'label' => 'Tag Icon',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('<i class="fa fa-info-circle"></i>
- Enter the link for your article then click “Check Link”');
  t('<i class="fa fa-info-circle"></i>
- Select the category here');
  t('<i class="fa fa-info-circle"></i>
- Where did the article come from? CNN? BBC?');
  t('<i class="fa fa-info-circle"></i>
- Write a title for all to see, this will give fellow users an idea of what to expect from your link');
  t('Hashtag Account Category');
  t('Hashtag Account Name');
  t('Hashtag Account hash');
  t('News Caption');
  t('News Category');
  t('News Fetch Account');
  t('News Fetch Date');
  t('News Hashtags');
  t('News Link');
  t('News Markup');
  t('News Media Link');
  t('News Retweets');
  t('News Source');
  t('Tag Icon');

  return $field_instances;
}
